// # Grammar
//
// ## Language
//
// Language is specified in BNF syntax. Some of the implicitly defined BNF (in not-necessarily
// valid BNF syntax) productions are:

<unicode>        ::= a valid UTF-8 encoded unicode codepoint
<unicode_wspace> ::= all unicode codepoints that are defined to be a whitespace
<without_null>   ::= <unicode> - '\u0000'
<without_eol>    ::= <unicode> - '\u000A'
<eol>            ::= '\u000A'
<without_apos>   ::= <unicode> - "'"
<without_quot>   ::= <unicode> - '"'
<ident>          ::= a valid identifier
					 (see
                     http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1518.htm
					 Appendix: [AltId] and [XML2008] illustrated
					 and Propossed Wording > Annex)
					 Characters indicated by a [box] are disallowed anywhere in the identifier.
					 ASCII Digits (\u0030 to \u0039) are disallowed innitially in addition to
					 those in Annex X of the document.
					 All other symbols are allowed in an identifier.

<reserved_op>    ::= "→" | "=" | ":" | ";" | "," | ".."
<infix>          ::= valid infix operator names (not <reserved_op>)

// Where `-` removes following terminal from preceding terminal or non-terminal
// We define some useful productions we will be using later:

<optional_wspace>   ::= <unicode_wspace> <optional_wspace> | ""
<some_wspace>       ::= <unicode_wspace> <optional_wspace>

// A program is a set of 1 or more items (at least the `main` function is mandatory), but it is not
// parser’s nor it is lexer’s responsibility to ensure that. Later compilation stages will take
// care of that instead.

<program>           ::= <item> <optional_wspace> <program> | <optional_wspace>
// NOTE: I am aware basic BNF does not allow splitting rule lines, but I can’t help it haha
<item>              ::= <doc_item> | <attr_item> | <type_item> | <enum_item>
                      | <trait_item> | <impl_item> | <let_item>

<doc_item>          ::= <inner_doc> | <outer_doc>
<outer_doc>         ::= <outer_doc_line> <outer_doc> | <outer_doc_line>
<outer_doc_line>    ::= "///" <without_eol> <eol>
<inner_doc>         ::= <inner_doc_line> <inner_doc> | <inner_doc_line>
<inner_doc_line>    ::= "//!" <without_eol> <eol>

<comment_item>      ::= "//" <without_eol> <eol>

<attr_item>         ::= <inner_attr> | <outer_attr>
<inner_attr>        ::= "#![" <optional_wspace> <attr_content> <optional_wspace> "]"
<outer_attr>        ::= "#[" <optional_wspace> <attr_content> <optional_wspace> "]"
<attr_content>      ::= <ident> | <ident> <attr_args>
<attr_args>         ::= " " <string_literal> <attr_args> | <string_literal>


<type_item>         ::= "type" <some_wspace> <ident> <optional_wspace> "{" <type_item_inner> "}"
                      | "type" <some_wspace> <ident> <optional_wspace> "{" <optional_wspace "}"
<type_item_inner>   ::= <optional_wspace> <type_item_inner> <optional_wspace>
                      | <outer_doc>
                      | <type_item_el> "," <type_item_inner>
                      | <type_item_el>


<type_item_el>      ::= <ident> <optional_wspace> ":" <optional_wspace> <let_type>

<enum_item>         ::= "enum" <some_wspace> <ident> <optional_wspace> "{" <enum_item_inner> "}"
                      | "enum" <some_wspace> <ident> <optional_wspace> "{" <optional_wspace> "}"
<enum_item_inner>   ::= <optional_wspace> <enum_item_inner> <optional_wspace>
                      | <outer_doc>
                      | <enum_item_el> "," <enum_item_inner>
                      | <enum_item_el>

<enum_item_el>      ::= <ident> " " <let_type> | <ident>

<trait_item>        ::= "trait" <some_wspace> <ident> <optional_wspace> "for" <some_wspace>
                        <trait_implers> <optional_wspace> "{" <trait_item_body> "}"
<trait_item_body>   ::= <trait_item_inner> | <optional_wspace>
<trait_implers>     ::= <let_type> <optional_wspace> "," <optional_wspace> <trait_implers> | <let_type>
<trait_item_inner>  ::= <optional_wspace> <trait_item_inner> <optional_wspace>
                      | <outer_doc>
                      | <outer_attr>
                      | <let_item>

<impl_item>         ::= "impl" <some_wspace> <ident> <some_wspace> "for" <some_wspace>
                        <trait_implers> <optional_wspace> "{" <impl_item_body> "}"
<impl_item_body>    ::= <impl_item_inner> | <optional_wspace>
<impl_item_inner>   ::= <optional_wspace> <impl_item_inner> <optional_wspace>
                      | <outer_doc>
                      | <outer_attr>
                      | <let_item>

<let_item>          ::= "let" <optional_letmut> <let_ident> <optional_wspace> ":" <optional_wspace>
                        <let_type> <optional_wspace> <optional_init>
<let_ident>         ::= <ident> | "`" <infix> "`"
<optional_init>     ::= "=" <optional_wspace> <statement> | ";"
<optional_letmut>   ::= <some_wspace> "mut" <some_wspace> | <some_wspace>
<let_type>          ::= <type> <optional_wspace> "→" <optional_wspace> <let_type> | <type>

<type>              ::= <ident> | <builtin_ty>
<builtin_ty>        ::= <array_ty> | <tuple_ty>
<array_ty>          ::= "[" <optional_wspace> <type> <optional_wspace> ";" <optional_wspace>
                        <natint_literal> <optional_wspace> "]"
                      | "[" <optional_wspace> <type> <optional_wspace> "]"
<tuple_ty>          ::= "(" <tuple_subtys> ")" | "(" <optional_wspace> ")"
<tuple_subtys>      ::= <optional_wspace> <type> <optional_wspace> "," <optional_wspace>
                        <tuple_subtys> <optional_wspace>
                      | <type>

<statement>         ::= <let_item> | <expression> ";"
<statements_ex>     ::= <statement> <statements_ex> | <statement> | <expression>
<expression>        ::= <literal_expr> | <lambda_expr> | <block_expr> | <while_expr> | <for_expr>
                      | <if_expr> | <continue_expr> | <break_expr> | <return_expr>
                      | <assignment_expr> | <call_expr>

<assignment_expr>   ::= <ident> <optional_wspace> "=" <optional_wspace> <expression>
                        <optional_wspace>
<return_expr>       ::= "return" <some_wspace> <expression>
<break_expr>        ::= "break"
<continue_expr>     ::= "continue"

<block_expr>        ::= "{" <optional_wspace> <statements_ex> <optional_wspace> "}"
                      | "{" <optional_wspace> "}"
<lambda_expr>       ::= "|" <optional_wspace> <lambda_args> <optional_wspace> "|" <optional_wspace>
                        <block_expr>
                      | "|" <optional_wspace> "|" <optional_wspace> <block_expr>
<lambda_args>       ::= <ident> <optional_wspace> "," <optional_wspace> <lambda_args> | <ident>

<condition>         ::= "(" <optional_wspace> <expression> <optional_wspace> ")"
<if_expr>           ::= "if" <some_wspace> <condition> <optional_wspace> <block_expr>
                        <optional_wspace> <optional_else>
<optional_else>     ::= "else" <some_wspace> <if_expr> | "else" <optional_wspace> <block_expr> | ""
<for_expr>          ::= "for" <optional_wspace> "(" <optional_wspace>
                            <ident> <some_wspace> "in" <some_wspace> <expression>
                            <optional_wspace> ")" <optional_wspace> <block_expr>
<while_expr>        ::= "while" <some_wspace> <condition> <optional_wspace> <block_expr>

// This is hard because this has to basically be a linear list of expressions because infix
// operators are also functions that may appear between two arguments and there’s no way to
// distinguish these yet.
// This is figured out during semantic analysis.

<call_expr>         ::= <intrinsics> | <call_expr_part> " " <call_expr> | <call_expr_part>
<call_expr_part>    ::= <infix> | <call_infixd_part> | <call_exprs>
<call_infixed_expr> ::= <optional_wspace> "`" <ident> "`" <optional_wspace>
<call_exprs>        ::= <literal_expr> | <lambda_expr> | <block_expr> | <ident>

<literal_expr>      ::= <literal>
<literal>           ::= <string_literal> | <char_literal> | <number_literal> | <bool_literal>
                      | <array_literal> | <tuple_literal>

<string_literal>    ::= "\u0022" <string_body> "\u0022"
                      | "\u0022\u0022"
<string_body>       ::= <string_body_char> <string_body>
<string_body_char>  ::= <without_quot> | "\u005C" <string_escape>
<string_escape>     ::= "\u0022" | <named_escapes> | <unicode_escapes>

<char_literal>      ::= "\u0027" <char_body> "\u0027"
<char_body>         ::= <without_apos> | "\u005C" <char_escapes>
<char_escapes>      ::= "\u0027" | <named_escapes> | <unicode_escapes>

<number_literal>    ::= <nat_digit> <decimal_literal> <float_part>
                      | <nat_digit> <float_part>
                      | <natint_literal>
                      | <nat_digit>
                      | "0b" <binary_literal>
                      | "0o" <octal_literal>
                      | "0x" <hexa_literal>
                      | "0"
<decimal_literal>   ::= <decimal_literal_c> <decimal_literal>
<decimal_literal_c> ::= <dec_digit> | "_"
<natint_literal>    ::= <nat_digit> <decimal_literal> | <nat_digit>
<binary_literal>    ::= <binary_literal_c> <binary_literal> | <binary_literal_c>
<binary_literal_c>  ::= "1" | "0" | "_"
<octal_literal>     ::= <octal_literal_c> <octal_literal> | <octal_literal_c>
<octal_literal_c>   ::= <oct_digit> | "_"
<hexa_literal>      ::= <hexa_literal_c> <hexa_literal> | <hexa_literal_c>
<hexa_literal_c>    ::= <hex_digit> | "_"
<float_part>        ::= <exponent> | "." <decimal_literal> <exponent> | "." <exponent>
                      | "." <decimal_literal> | "."

<bool_literal>      ::= "true" | "false";

<array_literal>     ::= "[" <optional_wspace> <tuple_array_body> <optional_wspace> "]"
                      | "[" <optional_wspace> "]"

<tuple_literal>     ::= "(" <optional_wspace> <tuple_array_body> <optional_wspace> ")"
                      | "()"
<tuple_array_body>  ::= <expression> <optional_wspace> "," <optional_wspace> <tuple_array_body>
                      | <expression>

<nat_oct>           ::= "1" | "2" | "3" | "4" | "5" | "6" | "7"
<oct_digit>         ::= "0" | <nat_oct>
<nat_digit>         ::= <nat_oct> | "8" | "9"
<dec_digit>         ::= "0" | <nat_digit>
<hex_digit>         ::= <dec_digit> | "a" | "b" | "c" | "d" | "e" | "f"
                                    | "A" | "B" | "C" | "D" | "E" | "F"
<named_escapes>     ::= "\u005C" | "n" | "r" | "t" | "0" | "x" <hex_digit> <hex_digit>
<unicode_escapes>   ::= "u{" <unicode_escapes_b6>  "}";
<unicode_escapes_b6>::= <hex_digit> <hex_digit> <hex_digit> <hex_digit> <hex_digit> <hex_digit>
                      | <unicode_escapes_b5>
<unicode_escapes_b5>::= <hex_digit> <hex_digit> <hex_digit> <hex_digit> <hex_digit>
                      | <unicode_escapes_b4>
<unicode_escapes_b4>::= <hex_digit> <hex_digit> <hex_digit> <hex_digit> | <unicode_escapes_b3>
<unicode_escapes_b3>::= <hex_digit> <hex_digit> <hex_digit> | <unicode_escapes_b2>
<unicode_escapes_b2>::= <hex_digit> <hex_digit> | <unicode_escapes_b1>
<unicode_escapes_b1>::= <hex_digit>

<intrinsics>		::= "intrinsic_print_byte " <intrPrintArg>
					  | "intrinsic_read_byte"
					  | "intrinsic_index_array " <ident> <intrIdxArg>
<intrPrintArg>		::= <byte_literal> | <ident>
<intrIdxArg>		::= <number_literal> | <ident>

// # ETC
// These are rules that are necessary for assignment but are not necessarily a part of this
// language or its grammar (note: similar rules will be implemented in this language in
// after-parsing stage but will be user-defined; that is the fixity of operators is not
// decided by the language but rather by the author)

<associative_expression> ::= <comp_expr>

<comp_op>           ::= "==" | "!=" | ">" | "<" | ">=" | "<="
<comp_expr>         ::= <addsub_expr> | <addsub_expr> <comp_op> <comp_expr>

<addsub_op>         ::= "+" | "-"
<addsub_expr>       ::= <muldiv_expr> | <muldiv_expr> <addsub_op> <addsub_expr>

<muldiv_op>         ::= "*" | "/" | "%"
<muldiv_expr>       ::= <associative_lit> | <associative_lit> <muldiv_op> <muldiv_expr>

<associative_lit>   ::= "(" <associative_expression> ")" | <literal_expr> | <block_expr>
                      | <call_expr> | "-" <associative_lit> | "!" <associative_lit>
