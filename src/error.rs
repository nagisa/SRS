use std::error::Error as ErrorT;
use parse::ops::Fixity;
use intern::InternedStr;

pub type Result<T> = ::std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    IoError(::std::io::Error),
    StrError(&'static str),
    LexerError(LexerErrorKind),
    ParserError(ParserErrorKind),
    ConflictingPrecedenceError(InternedStr, InternedStr, Fixity, Fixity, u8),
    // For errors that have already been reported
    Quiet
}

#[derive(Debug)]
pub enum LexerErrorKind {
    UnknownPrefix(String),
    NotToken,
}

#[derive(Debug)]
pub enum ParserErrorKind {
    UnexpectedToken
}

impl From<::std::io::Error> for Error {
    fn from(e: ::std::io::Error) -> Error {
        Error::IoError(e)
    }
}

impl From<&'static str> for Error {
    fn from(e: &'static str) -> Error {
        Error::StrError(e)
    }
}


impl ErrorT for Error {
    fn description(&self) -> &str {
        match self {
            &Error::IoError(ref e) => e.description(),
            &Error::StrError(e) => e,
            &Error::LexerError(LexerErrorKind::UnknownPrefix(ref s)) => &s,
            &Error::LexerError(LexerErrorKind::NotToken) => "not a token",
            &Error::ParserError(ParserErrorKind::UnexpectedToken) => "unexpected token",
            &Error::ConflictingPrecedenceError(..) =>
                "conflicting fixities between operators",
            &Error::Quiet =>
                "already reported"
        }
    }
}

impl ::std::fmt::Display for Error {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        match self {
            &Error::IoError(ref e) => ::std::fmt::Display::fmt(e, f),
            &Error::StrError(e) => ::std::fmt::Display::fmt(e, f),
            &Error::LexerError(LexerErrorKind::NotToken) =>
                ::std::fmt::Display::fmt("not a token", f),
            &Error::LexerError(LexerErrorKind::UnknownPrefix(ref s)) => {
                try!(::std::fmt::Display::fmt("unknown lexeme prefix: ", f));
                ::std::fmt::Display::fmt(s, f)
            },
            &Error::ParserError(ParserErrorKind::UnexpectedToken) => {
                ::std::fmt::Display::fmt("unexpected token", f)
            }
            &Error::ConflictingPrecedenceError(s1, s2, f1, f2, p) => {
                write!(f, "conflicting fixities between operators {} ({:?} {}) and {} ({:?} {})",
                       s1, f1, p, s2, f2, p)
            }
            &Error::Quiet => {
                Ok(())
            }
        }

    }
}

impl Error {
    pub fn is_quiet(&self) -> bool {
        if let &Error::Quiet = self {
            true
        } else {
            false
        }
    }
}
