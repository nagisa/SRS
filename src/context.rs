use std::path::PathBuf;
use std::collections::HashMap;
use intern::InternedStr;
use memmap::{self, Mmap};
use error::{Result, Error};
use std::str;
use std::ops;
use parse::ast;
use resolve;
use parse::ops::Operators;
use std::io::{self, Write};

pub struct Context {
    pub sources: Vec<Source>,
    pub spans: Vec<Span>,
    pub ast: Option<ast::Program>,
    pub errors: usize,
    pub operators: Operators,

    pub defs: HashMap<resolve::Name, resolve::Def>,
    pub reverse_names: HashMap<resolve::Name, InternedStr>
}

impl Default for Context {
    fn default() -> Context {
        Context {
            sources: Vec::new(),
            spans: Vec::new(),
            ast: None,
            errors: 0,
            operators: Default::default(),
            defs: HashMap::new(),
            reverse_names: HashMap::new()
        }
    }
}

impl Context {
    pub fn add_file<P: Into<PathBuf>>(&mut self, p: P) -> Result<SourceId> {
        let path = p.into();
        let map = try!(memmap::Mmap::open_path(&*path, memmap::Protection::Read));
        let id = self.sources.len();
        self.sources.push(Source {
            path: path,
            map: map,
        });
        Ok(SourceId(id))
    }
    pub fn source(&self, s: SourceId) -> &Source {
        &self.sources[s.0]
    }
    pub fn new_span(&mut self, sid: SourceId, lo: usize, hi: usize) -> SpanId {
        let id = self.spans.len();
        self.spans.push(Span {
            sid: sid,
            lo: lo,
            hi: hi
        });
        SpanId(id + 1)
    }
    pub fn span(&self, s: SpanId) -> &Span {
        &self.spans[s.0 - 1]
    }
    // trying to retrieve it will panic
    pub fn dummy_span(&self) -> SpanId {
        SpanId(0)
    }
    pub fn diagnostic<'a>(&'a mut self) -> DiagnosticBuilder<'a> {
        DiagnosticBuilder {
            ctx: self,
            ty: DiagnosticTy::Error,
            span: None
        }
    }
    pub fn end_if_errors(&mut self) -> Result<()> {
        if self.errors != 0 {
            let errors = self.errors;
            self.diagnostic().emit(&*format!("aborting due to {} previous error{}",
                                             errors, if errors != 1 { "s" } else {""}));
            return Err(Error::Quiet)
        } else {
            Ok(())
        }
    }
}

// Sources and spanning

pub struct Source {
    // TODO: line map for nicer span output
    path: PathBuf,
    map: Mmap
}

impl Source {
    pub fn text(&self) -> Result<&str> {
        Ok(try!(unsafe {
            str::from_utf8(self.map.as_slice())
                .map_err(|_| "source file is not utf-8")
        }))
    }
}

#[derive(Debug)]
pub struct Span {
    pub sid: SourceId,
    pub lo: usize,
    pub hi: usize
}

#[derive(Debug)]
pub struct Spanned<T> {
    pub t: T,
    pub span: SpanId
}

impl<T> Spanned<T> {
    pub fn new(t: T, sid: SpanId) -> Spanned<T> {
        Spanned {
            t: t,
            span: sid
        }
    }
}

impl<T: Clone> Clone for Spanned<T> {
    fn clone(&self) -> Spanned<T> {
        Spanned {
            span: self.span,
            t: self.t.clone()
        }
    }
}

impl<T: Copy> Copy for Spanned<T> {}

impl<T> ops::Deref for Spanned<T> {
    type Target = T;
    fn deref(&self) -> &T {
        &self.t
    }
}

impl<T> ops::DerefMut for Spanned<T> {
    fn deref_mut(&mut self) -> &mut T {
        &mut self.t
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct SpanId(usize);
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct SourceId(usize);

// Diagnostics
pub struct DiagnosticBuilder<'c> {
    ctx: &'c mut Context,
    ty: DiagnosticTy,
    span: Option<SpanId>,
}

impl<'c> DiagnosticBuilder<'c> {
    pub fn error(mut self) -> DiagnosticBuilder<'c> {
        self.ty = DiagnosticTy::Error;
        self
    }
    pub fn warning(mut self) -> DiagnosticBuilder<'c> {
        self.ty = DiagnosticTy::Warning;
        self
    }
    pub fn hint(mut self) -> DiagnosticBuilder<'c> {
        self.ty = DiagnosticTy::Hint;
        self
    }
    pub fn span(mut self, sp: SpanId) -> DiagnosticBuilder<'c> {
        self.span = Some(sp);
        self
    }
    pub fn emit(self, msg: &str) {
        if self.ty == DiagnosticTy::Error {
            self.ctx.errors += 1;
        }
        let err = io::stderr();
        let mut err = err.lock();
        write!(err, "{}: {}\n", self.ty, msg).unwrap();
        if let Some(sp) = self.span.map(|s| self.ctx.span(s)) {
            let text = &self.ctx.source(sp.sid).text().unwrap()[sp.lo..sp.hi];
            write!(err, "{}:{}\t", sp.lo, sp.hi).unwrap();
            self.set_color(&mut err, 255, 255, 100);
            write!(err, "{}", text.trim_right_matches('\n')).unwrap();
            self.reset_color(&mut err);
            write!(err, "\n").unwrap();
        }
    }
    fn set_color<W: Write>(&self, mut w: W, r: u8, g: u8, b: u8) {
        write!(w, "\x1b[48;2;{};{};{}m", r, g, b).unwrap();
    }
    fn reset_color<W: Write>(&self, mut w: W) {
        write!(w, "\x1b[0m").unwrap();
    }
}

#[derive(Debug, PartialEq, Eq)]
pub enum DiagnosticTy {
    Error,
    Warning,
    Hint
}

impl ::std::fmt::Display for DiagnosticTy {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        match *self {
            DiagnosticTy::Error => f.write_str("\x1b[31merror\x1b[0m"),
            DiagnosticTy::Warning => f.write_str("\x1b[33mwarning\x1b[0m"),
            DiagnosticTy::Hint => f.write_str("hint")
        }
    }
}
