pub type InternedStr = &'static str;

pub fn intern(st: &str) -> InternedStr {
    intern_string(String::from(st))
}

pub fn intern_string(mut st: String) -> InternedStr {
    st.shrink_to_fit();
    unsafe {
        let r = ::std::mem::transmute(&*st);
        ::std::mem::forget(st);
        r
    }
}
