#![feature(const_fn, unicode, associated_consts, iter_cmp)]
extern crate memmap;
use error::Result;

mod intern;
mod error;
mod parse;
mod context;
mod resolve;

use parse::ast_pass::ASTPass;

fn lexparse(ctx: &mut context::Context, sid: context::SourceId) -> Result<parse::ast::Program> {
    let lexer = try!(parse::lex::Lexer::new(ctx, sid).map_err(|_| error::Error::Quiet));
    parse::Parser::parse(ctx, lexer).map_err(|_| error::Error::Quiet)
}

fn run(ctx: &mut context::Context) -> Result<()> {
    let sid = try!(ctx.add_file("example.srs"));
    let mut tree = try!(lexparse(ctx, sid));
    let _ = parse::ast_pass::FixityCollectPass::new(ctx).walk_program(&mut tree);
    let _ = parse::ast_pass::ResolveUnparsedPass::new(ctx).walk_program(&mut tree);
    try!(ctx.end_if_errors());
    {
        resolve::ResolvePass::resolve(ctx, &mut tree);
    }
    try!(ctx.end_if_errors());
    println!("{:?}", tree);
    ctx.ast = Some(tree);
    Ok(())
}

fn main() {
    let mut ctx = context::Context::default();
    run(&mut ctx).unwrap_or_else(|e| {
        if !e.is_quiet() {
            ctx.diagnostic().error().emit(&*format!("{}", e));
        }
        ::std::process::exit(1);
    });
}
