use std::collections::HashMap;

use parse::ast;
use context;
use intern::InternedStr;

#[derive(Clone, Copy, Debug)]
pub enum PrimitiveKind {
    Signed(u8),
    Unsigned(u8),
    Str
}

const PRIMITIVES: [(&'static str, PrimitiveKind); 9] = [
    ("u8", PrimitiveKind::Unsigned(8)),
    ("u16", PrimitiveKind::Unsigned(16)),
    ("u32", PrimitiveKind::Unsigned(32)),
    ("u64", PrimitiveKind::Unsigned(64)),
    ("i8", PrimitiveKind::Signed(8)),
    ("i16", PrimitiveKind::Signed(16)),
    ("i32", PrimitiveKind::Signed(32)),
    ("i64", PrimitiveKind::Signed(64)),
    ("str", PrimitiveKind::Str)
];

#[derive(Debug)]
pub enum TypeKind {
    Enum,
    Struct,
    Primitive(PrimitiveKind),
    /// parametrisation of trait
    Param,
}

#[derive(Debug)]
pub enum Def {
    Type(TypeKind),
    Variable,
    Argument,
    Method {
        trt: Name,
        imps: Vec<()>,
    },
    Trait,
    // Undefined,
}

#[derive(Copy, Clone, PartialEq, Eq, Debug, Hash)]
pub struct Name(usize);

pub struct Scope {
    local_names: HashMap<InternedStr, Name>,
}

pub struct ResolvePass<'c> {
    c: &'c mut context::Context,

    name_allocator: usize,
    reverse_names: HashMap<Name, InternedStr>,
    defs: HashMap<Name, Def>,

    // Stack of currently active scopes
    scopes: Vec<Scope>,
}

impl<'c> ResolvePass<'c> {
    pub fn resolve(c: &'c mut context::Context, ast: &mut ast::Program) {
        let mut rp = ResolvePass {
            c: c,
            name_allocator: 1,
            reverse_names: HashMap::new(),
            defs: HashMap::new(),
            scopes: vec![Scope {
                local_names: HashMap::new(),
            }],
        };
        // Prefill primitive types
        for &(ref n, ref pk) in PRIMITIVES.iter() {
            rp.add_definition(&ast::Name_::Str(n), Def::Type(TypeKind::Primitive(*pk)));
        }
        ast.items.iter_mut().fold((), |_, x| rp.resolve_item(x));

        ::std::mem::swap(&mut rp.c.defs, &mut rp.defs);
        ::std::mem::swap(&mut rp.c.reverse_names, &mut rp.reverse_names);

    }

    fn find_name<'a>(&'a self, n: &ast::Name_) -> Option<Name> {
        for scope in self.scopes.iter().rev() {
            let r = self.find_local_name(n, scope);
            if r.is_some() { return r; }
        }
        None
    }

    fn find_local_name<'a>(&self, n: &ast::Name_, scope: &'a Scope) -> Option<Name> {
        match *n {
            ast::Name_::Str(n) => scope.local_names.get(n).cloned(),
            ast::Name_::Name(n) => Some(n)
        }
    }

    fn with_def<R, F: FnOnce(&mut Def) -> R>(&mut self, n: Name, f: F) -> R {
        let def = self.defs.get_mut(&n).expect("name has no associated def");
        f(def)
    }

    fn with_scope<R, F: FnOnce(&mut Self) -> R>(&mut self, f: F) -> R {
        self.scopes.push(Scope {
            local_names: HashMap::new()
        });
        let ret = f(self);
        // FIXME: not panic safe
        self.scopes.pop();
        ret
    }

    fn new_name(&mut self) -> Name {
        let id = self.name_allocator;
        self.name_allocator += 1;
        Name(id)
    }

    fn add_definition(&mut self, n: &ast::Name_, d: Def) -> Name {
        match *n {
            ast::Name_::Str(n) => {
                let nn = self.new_name();
                self.reverse_names.insert(nn, n);
                self.defs.insert(nn, d);
                let mut scope = self.scopes.last_mut().expect("no scope");
                scope.local_names.insert(n, nn);
                nn
            },
            ast::Name_::Name(_) => unimplemented!(),
        }
    }

    fn revert_name(&self, n: &ast::Name_) -> InternedStr {
        match *n {
            ast::Name_::Str(n) => n,
            ast::Name_::Name(n) => self.reverse_names[&n],
        }
    }

    fn resolve_item(&mut self, i: &mut ast::Item) {
        match *i {
            ast::Item::Impl(ref mut i) => {
                let n = self.find_name(&i.t.name.t);
                let trt_n = match n {
                    None => {
                        let n = self.revert_name(&i.t.name.t);
                        self.c.diagnostic().span(i.span).emit(
                            &*format!("implementing undefined trait {}", n)
                        );
                        return;
                    }
                    Some(n) => {
                        // Implementing defined trait, now what do we do???
                        // putting em into trait's def would make sense, no?
                        // TODO: resolve lets inside.
                        i.t.name.t.make_name(n);
                        n
                    }
                };
                for mut ty in i.t.tys.iter_mut() {
                    self.resolve_type(&mut ty);
                }
                for mut li in i.t.lets.iter_mut() {
                    self.resolve_type(&mut li.ty);
                    let n = self.find_name(li.name.as_ref());
                    match n {
                        None => {
                            let n = self.revert_name(&li.t.name.as_ref());
                            self.c.diagnostic().span(li.span).emit(
                                &*format!("implementing undeclared method {}", n)
                            );
                        }
                        Some(mn) => {
                            let trt = self.with_def(mn, |d| {
                                if let Def::Method { trt, .. } = *d {
                                    Some(trt)
                                } else {
                                    None
                                }
                            });
                            match trt {
                                None => {
                                    let n = self.revert_name(&li.t.name.as_ref());
                                    self.c.diagnostic().span(li.span).emit(
                                        &*format!("{} is not a method", n)
                                    );
                                }
                                Some(t) if t != trt_n => {
                                    let n = self.revert_name(&li.t.name.as_ref());
                                    let t_n = self.reverse_names[&t];
                                    let trt_n = self.reverse_names[&trt_n];
                                    self.c.diagnostic().span(li.span).emit(
                                        &*format!("{} is a method in trait {}, but this \
                                                  definition appears in impl for {}",
                                                  n, t_n, trt_n)
                                    );
                                }
                                Some(_) => {
                                    self.with_def(mn, |d| {
                                        if let Def::Method { ref mut imps, .. } = *d {
                                            imps.push(());
                                            li.t.name.as_mut().make_name(mn);
                                        } else { panic!() }
                                    });
                                }
                            }
                        }
                    }
                    if let Some(ref mut i) = li.t.init {
                        self.resolve_stmt(i);
                    }
                }
            },
            ast::Item::Trait(ref mut t) => {
                let n = self.find_name(&t.t.name.t);
                let trt_n = match n {
                    None => {
                        let n = self.add_definition(&t.t.name.t, Def::Trait);
                        t.t.name.t.make_name(n);
                        n
                    }
                    Some(on) => {
                        let n = self.revert_name(&t.t.name.t);
                        self.c.diagnostic().span(t.span).emit(
                            &*format!("conflicting definition of trait {}", n)
                        );
                        on
                    }
                };
                self.with_scope(|this| {
                    for mut ty in t.t.tys.iter_mut() {
                        // abstract type
                        if let ast::Type_::Named(ref mut n) = ty.t {
                            let nm = this.add_definition(&n, Def::Type(TypeKind::Param));
                            n.make_name(nm);
                        }
                    }
                    for mut l in t.t.lets.iter_mut() {
                        this.resolve_type(&mut l.t.ty);
                    }
                });
                for mut li in t.t.lets.iter_mut() {
                    // Each let has its type resolved. Now we must ensure these can get declared
                    let n = self.find_name(&li.t.name.as_ref());
                    match n {
                        None => {
                            let n = self.add_definition(&li.t.name.as_ref(), Def::Method {
                                trt: trt_n,
                                imps: Vec::new()
                            });
                            li.t.name.as_mut().make_name(n);
                        }
                        Some(_) => {
                            let n = self.revert_name(&li.t.name.as_ref());
                            self.c.diagnostic().span(li.span).emit(
                                &*format!("conflicting declaration of method {}", n)
                            );
                        }
                    }
                    if let Some(ref mut i) = li.t.init {
                        self.resolve_stmt(i);
                    }
                }
            },
            ast::Item::Type(ref mut ty) => {
                let n = self.find_name(&ty.t.name.t);
                match n {
                    None => {
                        let n = self.add_definition(&ty.t.name.t, Def::Type(TypeKind::Struct));
                        ty.t.name.t.make_name(n);
                    }
                    Some(_) => {
                        let n = self.revert_name(&ty.t.name.t);
                        self.c.diagnostic().span(ty.span).emit(
                            &*format!("conflicting definition of type {}", n)
                        );
                    }
                }
                for mut field in ty.t.fields.iter_mut() {
                    // fields introduce implicit functions-accessors
                    let n = self.find_name(&field.name.t);
                    match n {
                        None => {
                            // FIXME: is this Def good?
                            let n = self.add_definition(&field.name.t, Def::Variable);
                            field.name.t.make_name(n);
                        }
                        Some(_) => {
                            let n = self.revert_name(&field.name.t);
                            self.c.diagnostic().span(field.span).emit(
                                &*format!("conflicting definition of field accessor {}", n)
                            );
                        }
                    }
                    self.resolve_type(&mut field.ty);
                }
            }
            ast::Item::Enum(ref mut e) => {
                let n = self.find_name(&e.t.name.t);
                match n {
                    None => {
                        let n = self.add_definition(&e.t.name.t, Def::Type(TypeKind::Enum));
                        e.t.name.t.make_name(n);
                    }
                    Some(_) => {
                        let n = self.revert_name(&e.t.name.t);
                        self.c.diagnostic().span(e.span).emit(
                            &*format!("conflicting definition of type {}", n)
                        );
                    }
                }
                // Variants introduce implicit functions-constructors
                for mut variant in e.t.variants.iter_mut() {
                    let n = self.find_name(&variant.name.t);
                    match n {
                        None => {
                            // FIXME: is this Def good?
                            let n = self.add_definition(&variant.name.t, Def::Variable);
                            variant.name.t.make_name(n);
                        }
                        Some(_) => {
                            let n = self.revert_name(&variant.name.t);
                            self.c.diagnostic().span(variant.span).emit(
                                &*format!("conflicting definition of variant constructor {}", n)
                            );
                        }
                    }
                    variant.contained_ty.as_mut().map(|t| {
                        self.resolve_type(t);
                    });
                }
            },
            ast::Item::Let(ref mut li) => self.resolve_letitem(li),
        }
    }

    fn resolve_stmt(&mut self, s: &mut ast::Stmt) {
        match s.t {
            ast::Stmt_::Let(ref mut li) => self.resolve_letitem(&mut *li),
            ast::Stmt_::Expr(ref mut ex) => self.resolve_expr(&mut *ex)
        }
    }

    fn resolve_letitem(&mut self, li: &mut ast::LetItem) {
        let n = {
            let scope = self.scopes.last().expect("no scope");
            self.find_local_name(&li.t.name.as_ref(), scope)
        };
        match n {
            None => {
                let n = self.add_definition(&li.t.name.as_ref(), Def::Variable);
                li.t.name.as_mut().make_name(n);
            }
            Some(_) => {
                let n = self.revert_name(&li.t.name.as_ref());
                self.c.diagnostic().span(li.span).emit(
                    &*format!("conflicting definition of let {}", n)
                );
            }
        }
        self.resolve_type(&mut li.t.ty);
        if let Some(ref mut i) = li.init {
            self.resolve_stmt(i);
        }
    }

    fn resolve_expr(&mut self, e: &mut ast::Expr) {
        match e.t {
            ast::Expr_::Block(ref mut stmts, ref mut expr) => {
                // blocks always introduce a new scope
                self.with_scope(|this| {
                    for mut stmt in stmts.iter_mut() {
                        this.resolve_stmt(stmt);
                    }
                    if let Some(ref mut ex) = *expr {
                        this.resolve_expr(ex);
                    }
                });
            },
            ast::Expr_::Lambda(ref mut args, ref mut block) => {
                // we introduce a scope for arguments…
                self.with_scope(|this| {
                    for mut arg in args.iter_mut() {
                        let n = {
                            let scope = this.scopes.last().expect("no scope");
                            this.find_local_name(&arg, scope)
                        };
                        if let Some(_) = n {
                            let n = this.revert_name(arg);
                            this.c.diagnostic().span(arg.span).emit(
                                &*format!("argument {} named multiple times", n)
                            );
                        } else {
                            let n = this.add_definition(arg, Def::Argument);
                            arg.make_name(n);
                        }

                    }
                    this.resolve_expr(block);
                });
            },
            ast::Expr_::If {ref mut cond, ref mut body, ref mut elbr} => {
                self.resolve_expr(cond);
                self.resolve_expr(body);
                if let Some(ref mut elbr) = *elbr {
                    self.resolve_expr(elbr);
                }
            },
            ast::Expr_::While {ref mut cond, ref mut body} => {
                self.resolve_expr(cond);
                self.resolve_expr(body);
            },
            ast::Expr_::For {ref mut bind, ref mut iter, ref mut body} => {
                self.resolve_expr(iter);
                self.with_scope(|this| {
                    let n = this.add_definition(bind, Def::Variable);
                    bind.make_name(n);
                    this.resolve_expr(body);
                });
            },
            ast::Expr_::Return(ref mut expr) => {
                self.resolve_expr(expr);
            },
            ast::Expr_::Ident(ref mut n) => {
                if let Some(s) = self.find_name(n) {
                    n.make_name(s);
                } else {
                    let name = self.revert_name(n);
                    self.c.diagnostic().span(n.span).emit(
                        &*format!("{} is undeclared", name)
                    );
                }
            },
            ast::Expr_::Literal(ref mut l) => {
                self.resolve_literal(l);
            },
            ast::Expr_::Assign(ref mut ex1, ref mut ex2) => {
                self.resolve_expr(ex1);
                self.resolve_expr(ex2);
            },
            ast::Expr_::CallTree(ref mut ex1, ref mut exs) => {
                self.resolve_expr(ex1);
                for ex in exs.iter_mut() {
                    self.resolve_expr(ex);
                }
            },
            ast::Expr_::CallExpr(_) => panic!("WHAT?!"),
            ast::Expr_::Continue | ast::Expr_::Break => (),
        }
    }

    fn resolve_literal(&mut self, l: &mut ast::Literal) {
        match l.t {
            ast::Literal_::Tuple(ref mut exprs) | ast::Literal_::Array(ref mut exprs) => {
                for mut expr in exprs.iter_mut() {
                    self.resolve_expr(&mut expr);
                }
            }
            ast::Literal_::Str(_) | ast::Literal_::Char(_) | ast::Literal_::Int(_) |
            ast::Literal_::Float(_) | ast::Literal_::Bool(_) => (),
        }
    }

    fn resolve_type(&mut self, i: &mut ast::Type) {
        match i.t {
            ast::Type_::Null => {},
            ast::Type_::Tuple(ref mut tys) => {
                for mut ty in tys.iter_mut() {
                    self.resolve_type(&mut ty);
                }
            }
            ast::Type_::Array(ref mut ty, _) => {
                self.resolve_type(ty);
            }
            ast::Type_::Slice(ref mut ty) => {
                self.resolve_type(ty);
            }
            ast::Type_::Named(ref mut n) => {
                let foundn = self.find_name(&n);
                match foundn {
                    None => {
                        let n = self.revert_name(&n);
                        self.c.diagnostic().span(i.span).emit(
                            &*format!("use of undefined type {}", n)
                        );
                    },
                    Some(name) => {
                        let is_ty = self.with_def(name, |d| {
                            match *d {
                                Def::Type(..) => true,
                                _ => false
                            }
                        });
                        if !is_ty {
                            let n = self.revert_name(&n);
                            self.c.diagnostic().span(i.span).emit(
                                &*format!("{} is not a type", n)
                            );
                        } else {
                            n.make_name(name);
                        }
                    }
                }
            }
            ast::Type_::To(ref mut ty1, ref mut ty2) => {
                self.resolve_type(&mut *ty1);
                self.resolve_type(&mut *ty2);
            }
        }
    }
}
