use intern;
use error::{Result, Error};

pub struct Operators {
    ops: Vec<Operator>
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct Operator {
    pub precedence: u8,
    pub fixity: Fixity,
    pub symbol: intern::InternedStr,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Fixity {
    Left,
    Right,
    None
}

impl Default for Operators {
    fn default() -> Self {
        Operators {
            ops: vec![Operator {
                precedence: 0,
                fixity: Fixity::Right,
                symbol: "="
            }]
        }
    }
}

impl Operators {
    pub fn add_operator(&mut self, symbol: intern::InternedStr, fixity: Fixity, precedence: u8)
    -> Result<()> {
        for op in &self.ops {
            if precedence == op.precedence && op.fixity != fixity {
                return Err(Error::ConflictingPrecedenceError(symbol, op.symbol,
                                                             fixity, op.fixity,
                                                             precedence));
            }
        }
        self.ops.push(Operator {
            symbol: symbol,
            fixity: fixity,
            precedence: precedence
        });
        Ok(())
    }

    pub fn get_operator(&mut self, symbol: intern::InternedStr) -> Result<Operator> {
        self.ops.iter().find(|x| x.symbol == symbol).cloned().map(Ok)
            .unwrap_or(Err("unknown operator".into()))
    }
}
