//! Parsing code
//!
//! Due to the nature of SRS syntax, parsing happens in two distinct stages. Firstly all items are
//! parsed and then store their “bodies” as regular token trees.
//! Then the table of operators and their fixities are collected (fixity attribute) with which
//! parsing is continued to fully parse various associative expressions inside the item bodies.
pub mod ast;
pub mod ast_pass;
pub mod ops;
pub mod lex;
pub mod altid;

use context::{self, Spanned, SpanId};
use intern::{self, InternedStr};
use parse::ops::Fixity;

use self::lex::{Token, LexerIterator, KeywordToken, SpaceKind, DelimiterToken, LiteralToken};
use error::{Result, Error, ParserErrorKind};
use std::borrow::Cow;
use std::char;
use std::fmt::Write;


#[derive(Debug, PartialEq, Eq)]
enum Expectation {
    /// exact token
    Token(lex::Token),
    /// some whitespace
    SomeWhitespace,
    /// an identifier
    Ident,
    /// a literal
    Literal,
    /// number literal
    Number,
    /// space followed by a string literal
    AttributeArgument,
    /// some sort of symbol (Token::Symbol(_))
    Symbol,
    /// item keywords
    Item,
    Documentation(bool),
}



pub struct Parser<'a, I: LexerIterator> {
    ctx: &'a mut context::Context,
    lexer: I,
    current: context::Spanned<lex::Token>,
    next: context::Spanned<lex::Token>,
    expectations: Vec<Expectation>,
    token_collector: Option<Vec<context::Spanned<lex::Token>>>,
}

macro_rules! add_expectation {
    ($s:ident, $p: expr) => ($s.expectations.push(Expectation::Token($p)))
}

macro_rules! _tt_as_expr_hack {
    ($value:expr) => ($value)
}

macro_rules! eat_token {
    ($s: ident, ~$($p:tt)+) => {
        _tt_as_expr_hack! {
            match $s.current.t {
                $($p)+ => {
                    try!($s.bump());
                    Ok(())
                }
                _ => {
                    $s.unexpected()
                }
            }
        }
    };
    ($s: ident, $($p:tt)+) => {
        _tt_as_expr_hack! {
            match $s.current.t {
                $($p)+ => {
                    $s.bump()
                }
                _ => {
                    add_expectation!($s, $($p)+);
                    if let $($p)+ = $s.next.t {
                        let _: Result<()> = $s.unexpected();
                        // remove lexeme in between and parse ahead
                        try!($s.bump());
                        $s.bump()
                    } else {
                        // insert lexeme and parse ahead, looking for more errors
                        // hopefully this insertion wont make false positives. Maybe should emit a
                        // warning?
                        let _: Result<()> = $s.unexpected();
                        Ok(())
                    }
                }
            }
        }
    }
}

impl<'a, I: LexerIterator> Parser<'a, I> {
    pub fn parse(ctx: &'a mut context::Context, lexer: I) -> Result<ast::Program> {
        let mut lexer = lexer.filter(|t| {
            match t.t {
                Token::Comment(_) => false,
                _ => true
            }
        });
        let current = try!(lexer.next(ctx));
        let next = try!(lexer.next(ctx));
        let mut parser = Parser {
            ctx: ctx,
            lexer: lexer,
            current: current,
            next: next,
            expectations: Vec::new(),
            token_collector: None,
        };
        parser.parse_program()
    }

    /// Bumps token state only (i.e. does not clear expectations)
    fn bump_tokens(&mut self) -> Result<()> {
        let mut next = try!(self.lexer.next(self.ctx));
        // Don’t bother with repeating whitespace (may happen after comments are filtered)
        while let (&Token::Whitespace(_), &Token::Whitespace(_)) = (&next.t, &self.next.t) {
            next = try!(self.lexer.next(self.ctx));
        }
        ::std::mem::swap(&mut self.current, &mut self.next);
        let previous = ::std::mem::replace(&mut self.next, next);
        self.token_collector.as_mut().map(move|v| v.push(previous));
        Ok(())
    }

    fn bump(&mut self) -> Result<()> {
        try!(self.bump_tokens());
        self.expectations.clear();
        Ok(())
    }

    fn parse_program(&mut self) -> Result<ast::Program> {
        let mut items = Vec::new();
        self.with_attributes(false, |this, attrs| {
            while this.current.t != Token::EOF {
                match this.current.t {
                    // Ignore whitespace between items
                    Token::Whitespace(_) => {
                        try!(this.bump());
                        continue
                    },
                    _ => {
                        let item = try!(this.parse_item());
                        items.push(item)
                    }
                }
            }
            try!(eat_token!(this, Token::EOF)); // ensure nobody prematurely exits the loop above
            Ok(ast::Program {
                attributes: attrs,
                items: items
            })
        })
    }

    /// Generates a best keyword for current token
    fn best_keyword(&mut self, set_current: bool) -> Option<KeywordToken> {
        match self.current.t {
            Token::Keyword(k) => Some(k),
            Token::Ident(i) => {
                // since the error has been already reported, set the current token to whatever
                // trait we return
                let _: Result<()> = self.unexpected();
                KeywordToken::best_from_str(i).map(|(_, k)| {
                    if set_current {
                        self.current.t = Token::Keyword(k);
                        self.ctx.diagnostic().hint().emit(&*format!("replaced with keyword {:?}", k));
                    }
                    k
                })
            }
            _ => None
        }
    }

    fn parse_item(&mut self) -> Result<ast::Item> {
        self.with_attributes(true, |this, attrs| {
            this.expectations.push(Expectation::Item);
            match this.best_keyword(true) {
                Some(KeywordToken::Type) => this.parse_type_item(attrs),
                Some(KeywordToken::Enum) => this.parse_enum_item(attrs),
                Some(KeywordToken::Let) => this.parse_let_item(attrs).map(ast::Item::Let),
                Some(KeywordToken::Trait) => this.parse_trait_item(attrs),
                Some(KeywordToken::Impl) => this.parse_impl_item(attrs),
                _ => {
                    let r = this.unexpected();
                    try!(this.bump());
                    match this.best_keyword(true) {
                        Some(KeywordToken::Type) => this.parse_type_item(attrs),
                        Some(KeywordToken::Enum) => this.parse_enum_item(attrs),
                        Some(KeywordToken::Let) => this.parse_let_item(attrs)
                                                       .map(ast::Item::Let),
                        Some(KeywordToken::Trait) => this.parse_trait_item(attrs),
                        Some(KeywordToken::Impl) => this.parse_impl_item(attrs),
                        _ => r
                    }
                }
            }
        })
    }

    /// Parses the product types (`type <ident> {…}`)
    fn parse_type_item(&mut self, attrs: Vec<ast::Attribute>) -> Result<ast::Item> {
        let lo = self.current.span;
        try!(eat_token!(self, Token::Keyword(KeywordToken::Type)));
        try!(self.parse_space());
        let i = try!(self.parse_ident());
        try!(self.optional_space());
        try!(eat_token!(self, Token::Opening(DelimiterToken::Brace)));
        let mut fields: Vec<ast::TypeItemField> = Vec::new();
        loop {
            try!(self.optional_space());
            match self.current.t {
                Token::Closing(DelimiterToken::Brace) => break,
                _ => {
                    fields.push(try!(self.parse_type_item_field()));
                    if let Token::ResSymbol(",") = self.current.t {
                        try!(self.bump());
                    } else {
                        try!(self.optional_space());
                        break
                    }
                }
            }
        }
        let hi = self.current.span;
        try!(eat_token!(self, Token::Closing(DelimiterToken::Brace)));
        Ok(ast::Item::Type(self.mk_span(lo, hi, ast::TypeItem_ {
            name: i,
            fields: fields,
            attributes: attrs
        })))
    }

    fn parse_type_item_field(&mut self) -> Result<ast::TypeItemField> {
        self.with_attributes(true, |this, attrs| {
            try!(this.optional_space());
            let name = try!(this.parse_ident());
            try!(this.optional_space());
            try!(eat_token!(this, Token::ResSymbol(":")));
            try!(this.optional_space());
            let ty = try!(this.parse_type());
            Ok(this.mk_span(name.span, ty.span, ast::TypeItemField_ {
                attributes: attrs,
                name: name,
                ty: ty
            }))
        })
    }

    /// Parses the sum types `enum <ident> {…}`.
    fn parse_enum_item(&mut self, attrs: Vec<ast::Attribute>) -> Result<ast::Item> {
        let lo = self.current.span;
        try!(eat_token!(self, Token::Keyword(KeywordToken::Enum)));
        try!(self.parse_space());
        let i = try!(self.parse_ident());
        try!(self.optional_space());
        try!(eat_token!(self, Token::Opening(DelimiterToken::Brace)));
        let mut variants: Vec<ast::EnumItemVariant> = Vec::new();
        loop {
            try!(self.optional_space());
            match self.current.t {
                Token::Closing(DelimiterToken::Brace) => break,
                _ => {
                    variants.push(try!(self.parse_enum_item_variant()));
                    if let Token::ResSymbol(",") = self.current.t {
                        try!(self.bump());
                    } else {
                        try!(self.optional_space());
                        break
                    }
                }
            }
        }
        let hi = self.current.span;
        try!(eat_token!(self, Token::Closing(DelimiterToken::Brace)));
        Ok(ast::Item::Enum(self.mk_span(lo, hi, ast::EnumItem_ {
            name: i,
            variants: variants,
            attributes: attrs
        })))
    }

    fn parse_enum_item_variant(&mut self) -> Result<ast::EnumItemVariant> {
        self.with_attributes(true, |this, attrs| {
            try!(this.optional_space());
            let lo = this.current.span;
            let hi = this.current.span;
            let name = try!(this.parse_ident());
            let (hi, ty) = if this.is_like_type(&this.next.t) {
                if let Token::Whitespace(SpaceKind::Space) = this.current.t {
                    try!(this.parse_space());
                    let ty = try!(this.parse_type());
                    (ty.span, Some(ty))
                } else {
                    (hi, None)
                }
            } else {
                (hi, None)
            };
            Ok(this.mk_span(lo, hi, ast::EnumItemVariant_ {
                attributes: attrs,
                name: name,
                contained_ty: ty
            }))
        })
    }

    /// Parses the trait items `trait <ident> for <types> {…}`.
    fn parse_let_item(&mut self, attrs: Vec<ast::Attribute>) -> Result<ast::LetItem> {
        let lo = self.current.span;
        try!(eat_token!(self, Token::Keyword(KeywordToken::Let)));
        try!(self.parse_space());
        let is_mutable = if let Token::Keyword(KeywordToken::Mut) = self.current.t {
            try!(self.bump());
            try!(self.parse_space());
            true
        } else {
            false
        };
        let i = if let Token::ResSymbol("`") = self.current.t {
            ast::LetName::Infix(try!(self.parse_infix()))
        } else {
            ast::LetName::Regular(try!(self.parse_ident()))
        };
        try!(self.optional_space());
        try!(eat_token!(self, Token::ResSymbol(":")));
        try!(self.optional_space());
        let ty = try!(self.parse_type());
        try!(self.optional_space());
        self.expectations.push(Expectation::Token(Token::ResSymbol("=")));
        self.expectations.push(Expectation::Token(Token::ResSymbol(";")));
        match self.current.t {
            Token::ResSymbol("=") => {
                try!(self.bump());
                try!(self.optional_space());
                let stmt = try!(self.parse_stmt());
                Ok(self.mk_span(lo, stmt.span, ast::LetItem_ {
                    attributes: attrs,
                    mutable: is_mutable,
                    name: i,
                    ty: ty,
                    init: Some(stmt)
                }))
            }
            Token::ResSymbol(";") => {
                let hi = self.current.span;
                try!(self.bump());
                Ok(self.mk_span(lo, hi, ast::LetItem_ {
                    attributes: attrs,
                    mutable: is_mutable,
                    name: i,
                    ty: ty,
                    init: None
                }))
            }
            _ => self.unexpected()
        }
    }

    /// Parses the trait items `trait <ident> for <types> {…}`.
    fn parse_trait_item(&mut self, attrs: Vec<ast::Attribute>) -> Result<ast::Item> {
        let lo = self.current.span;
        try!(eat_token!(self, Token::Keyword(KeywordToken::Trait)));
        try!(self.parse_space());
        let tname = try!(self.parse_ident());
        try!(self.parse_space());
        try!(eat_token!(self, Token::Keyword(KeywordToken::For)));
        try!(self.parse_space());
        let types = try!(self.parse_trait_types());
        try!(self.optional_space());
        try!(eat_token!(self, Token::Opening(DelimiterToken::Brace)));
        let mut lets = Vec::new();
        loop {
            try!(self.optional_space());
            if let Token::Closing(DelimiterToken::Brace) = self.current.t {
                break
            }
            lets.push(try!(self.with_attributes(true, |this, attrs|
                this.parse_let_item(attrs)
            )));
        }
        let hi = self.current.span;
        try!(eat_token!(self, Token::Closing(DelimiterToken::Brace)));
        Ok(ast::Item::Trait(self.mk_span(lo, hi, ast::TraitItem_ {
            attributes: attrs,
            lets: lets,
            name: tname,
            tys: types
        })))
    }

    /// Parses the trait items `impl <ident> for <types> {…}`.
    fn parse_impl_item(&mut self, attrs: Vec<ast::Attribute>) -> Result<ast::Item> {
        let lo = self.current.span;
        try!(eat_token!(self, Token::Keyword(KeywordToken::Impl)));
        try!(self.parse_space());
        let tname = try!(self.parse_ident());
        try!(self.parse_space());
        try!(eat_token!(self, Token::Keyword(KeywordToken::For)));
        try!(self.parse_space());
        let types = try!(self.parse_trait_types());
        try!(self.optional_space());
        try!(eat_token!(self, Token::Opening(DelimiterToken::Brace)));
        let mut lets = Vec::new();
        loop {
            try!(self.optional_space());
            if let Token::Closing(DelimiterToken::Brace) = self.current.t {
                break
            }
            lets.push(try!(self.with_attributes(true, |this, attrs|
                this.parse_let_item(attrs)
            )));
        }
        let hi = self.current.span;
        try!(eat_token!(self, Token::Closing(DelimiterToken::Brace)));
        Ok(ast::Item::Impl(self.mk_span(lo, hi, ast::ImplItem_ {
            attributes: attrs,
            lets: lets,
            name: tname,
            tys: types
        })))
    }

    fn parse_stmt(&mut self) -> Result<ast::Stmt> {
        if let ast::StmtExpr::Stmt(s) = try!(self.parse_stmt_expr()) {
            Ok(s)
        } else {
            self.unexpected()
        }
    }

    /// Parses a statement expression (statement without `;` at the end). Note that the optional
    /// `;` does not apply to let statements. They’re always a statement.
    fn parse_stmt_expr(&mut self) -> Result<ast::StmtExpr> {
        match self.current.t {
            Token::Keyword(KeywordToken::Let) => {
                let it = try!(self.parse_let_item(Vec::new()));
                Ok(ast::StmtExpr::Stmt(Spanned {
                    span: it.span,
                    t: ast::Stmt_::Let(Box::new(it))
                }))
            }
            _ => {
                let expr = try!(self.parse_expr());
                try!(self.optional_space());
                self.expectations.push(Expectation::Token(Token::ResSymbol(";")));
                if let Token::ResSymbol(";") = self.current.t {
                    try!(self.bump());
                    Ok(ast::StmtExpr::Stmt(Spanned {
                        span: expr.span,
                        t: ast::Stmt_::Expr(Box::new(expr))
                    }))
                } else {
                    Ok(ast::StmtExpr::Expr(expr))
                }
            }
        }
    }

    /// Parse an expression, as much as possible, leaving currently unparseable bits in token-list
    /// state. These tokens should be later parsed again when associativity information is
    /// available.
    fn parse_expr(&mut self) -> Result<ast::Expr> {
        match self.current.t {
            Token::Keyword(KeywordToken::Continue) => {
                let span = self.current.span;
                try!(self.bump());
                Ok(Spanned {
                    span: span,
                    t: ast::Expr_::Continue
                })
            }
            Token::Keyword(KeywordToken::Break) => {
                let span = self.current.span;
                try!(self.bump());
                Ok(Spanned {
                    span: span,
                    t: ast::Expr_::Break
                })
            }
            Token::Keyword(KeywordToken::Return) => {
                let lo = self.current.span;
                try!(self.bump());
                let ex = try!(self.parse_expr());
                Ok(self.mk_span(lo, ex.span, ast::Expr_::Return(Box::new(ex))))
            }
            Token::Keyword(KeywordToken::If) => self.parse_if_expr(),
            Token::Keyword(KeywordToken::While) => {
                let lo = self.current.span;
                try!(self.bump());
                try!(self.parse_space());
                let cond = try!(self.parse_condition());
                try!(self.optional_space());
                let body = try!(self.parse_block_expr());
                Ok(self.mk_span(lo, body.span, ast::Expr_::While {
                    cond: Box::new(cond),
                    body: Box::new(body)
                }))
            }
            Token::Keyword(KeywordToken::For) => {
                let lo = self.current.span;
                try!(self.bump());
                try!(self.optional_space());
                try!(eat_token!(self, Token::Opening(DelimiterToken::Paren)));
                try!(self.optional_space());
                let binding = try!(self.parse_ident());
                try!(self.some_space());
                try!(eat_token!(self, Token::Keyword(KeywordToken::In)));
                try!(self.some_space());
                let iter = try!(self.parse_expr());
                try!(self.optional_space());
                try!(eat_token!(self, Token::Closing(DelimiterToken::Paren)));
                try!(self.optional_space());
                let body = try!(self.parse_block_expr());
                Ok(self.mk_span(lo, body.span, ast::Expr_::For {
                    bind: binding,
                    iter: Box::new(iter),
                    body: Box::new(body)
                }))
            }
            _ => if self.token_collector.is_none() {
                let lo = self.current.span;
                self.token_collector = Some(Vec::new());
                let r = self.parse_call_assign_expr();
                let v = self.token_collector.take().unwrap();
                try!(r);
                let hi = self.current.span;
                Ok(self.mk_span(lo, hi, ast::Expr_::CallExpr(v)))
            } else {
                let lo = self.current.span;
                self.parse_call_assign_expr().map(|_| {
                    let hi = self.current.span;
                    self.mk_span(lo, hi, ast::Expr_::CallExpr(Vec::new()))
                 })
            }
        }
    }

    fn parse_call_assign_expr(&mut self) -> Result<()> {
        try!(self.parse_expr_component());
        match (&self.current.t, &self.next.t) {
            (&Token::Whitespace(_), &Token::ResSymbol("=")) => {
                try!(self.optional_space());
                try!(self.bump());
                try!(self.optional_space());
                try!(self.parse_expr());
                Ok(())
            }
            (&Token::ResSymbol("="), _) => {
                try!(self.bump());
                try!(self.optional_space());
                try!(self.parse_expr());
                Ok(())
            }
            _ => {
                loop {
                    match self.current.t {
                        Token::Whitespace(SpaceKind::Space) => {
                            try!(self.bump());
                        }
                        Token::ResSymbol("|") | Token::ResSymbol("||") => {
                            try!(self.parse_lambda_expr());
                        }
                        Token::ResSymbol(_) => break,
                        Token::Symbol(_) => {
                            try!(self.bump());
                        }
                        Token::Ident(_) => {
                            try!(self.bump());
                        }
                        Token::Opening(DelimiterToken::Brace) => {
                            try!(self.parse_block_expr());
                        }
                        l => {
                            if self.may_start_literal(&l) {
                                try!(self.parse_literal());
                            } else {
                                break
                            }
                        }
                    }
                }
                Ok(())
            }
        }
    }

    fn parse_expr_component(&mut self) -> Result<()> {
        self.expectations.push(Expectation::Symbol);
        self.expectations.push(Expectation::Ident);
        self.expectations.push(Expectation::Token(Token::Opening(DelimiterToken::Brace)));
        self.expectations.push(Expectation::Token(Token::ResSymbol("|")));
        self.expectations.push(Expectation::Literal);
        match self.current.t {
            Token::Symbol(_) => {
                self.bump()
            }
            Token::Opening(DelimiterToken::Brace) => self.parse_block_expr().map(|_| ()),
            Token::ResSymbol("|") => self.parse_lambda_expr().map(|_| ()),
            Token::Ident(_) => {
                try!(self.parse_ident());
                Ok(())
            }
            _ => if self.may_start_literal(&self.current.t) {
                try!(self.parse_literal());
                Ok(())
            } else {
                self.unexpected()
            }
        }
    }

    fn parse_block_expr(&mut self) -> Result<ast::Expr> {
        let lo = self.current.span;
        try!(eat_token!(self, Token::Opening(DelimiterToken::Brace)));
        let mut stmts = Vec::new();
        let mut expr = None;
        loop {
            try!(self.optional_space());
            if !self.may_start_expr(&self.current.t) { break; }
            match try!(self.parse_stmt_expr()) {
                ast::StmtExpr::Stmt(s) => stmts.push(s),
                ast::StmtExpr::Expr(e) => {
                    expr = Some(Box::new(e));
                    break;
                }
            }
        }
        let hi = self.current.span;
        try!(eat_token!(self, Token::Closing(DelimiterToken::Brace)));
        Ok(self.mk_span(lo, hi, ast::Expr_::Block(stmts, expr)))
    }

    fn parse_lambda_expr(&mut self) -> Result<ast::Expr> {
        self.expectations.push(Expectation::Token(Token::ResSymbol("|")));
        match self.current.t {
            Token::ResSymbol("|") => {
                let lo = self.current.span;
                try!(self.bump());
                let mut args = Vec::new();
                try!(self.optional_space());
                self.expectations.push(Expectation::Ident);
                self.expectations.push(Expectation::Token(Token::ResSymbol("|")));
                match self.current.t {
                    Token::ResSymbol("|") => {},
                    Token::Ident(_) => {
                        args.push(try!(self.parse_ident()));
                        loop {
                            try!(self.optional_space());
                            self.expectations.push(Expectation::Token(Token::ResSymbol(",")));
                            if let Token::ResSymbol(",") = self.current.t {
                                try!(self.bump());
                                try!(self.optional_space());
                                args.push(try!(self.parse_ident()));
                            } else {
                                break
                            }
                        }
                    }
                    _ => return self.unexpected()
                }
                try!(eat_token!(self, Token::ResSymbol("|")));
                try!(self.optional_space());
                let block = try!(self.parse_block_expr());
                Ok(self.mk_span(lo, block.span, ast::Expr_::Lambda(args, Box::new(block))))
            },
            _ => self.unexpected()
        }
    }

    fn parse_if_expr(&mut self) -> Result<ast::Expr> {
        let lo = self.current.span;
        println!("ifexpr without if? {:?}", self.current);
        try!(eat_token!(self, Token::Keyword(KeywordToken::If)));
        try!(self.optional_space());
        let cond = try!(self.parse_condition());
        try!(self.optional_space());
        let block = try!(self.parse_block_expr());
        let hi = block.span;
        try!(self.optional_space());
        let (elsebr, hi) = if let Token::Keyword(KeywordToken::Else) = self.current.t {
            try!(self.bump());
            try!(self.optional_space());
            if let Token::Keyword(KeywordToken::If) = self.current.t {
                let ex = try!(self.parse_if_expr());
                let sp = ex.span;
                (Some(Box::new(ex)), sp)
            } else if let Token::Opening(DelimiterToken::Paren) = self.current.t {
                let ex = try!(self.parse_if_expr());
                let sp = ex.span;
                (Some(Box::new(ex)), sp)
            } else {
                let ex = try!(self.parse_block_expr());
                let sp = ex.span;
                (Some(Box::new(ex)), sp)
            }
        } else {
            (None, hi)
        };
        Ok(self.mk_span(lo, hi, ast::Expr_::If {
            cond: Box::new(cond),
            body: Box::new(block),
            elbr: elsebr
        }))
    }

    fn parse_condition(&mut self) -> Result<ast::Expr> {
        try!(eat_token!(self, Token::Opening(DelimiterToken::Paren)));
        try!(self.optional_space());
        let r = try!(self.parse_expr());
        try!(self.optional_space());
        try!(eat_token!(self, Token::Closing(DelimiterToken::Paren)));
        Ok(r)
    }

    fn may_start_literal(&self, t: &Token) -> bool {
        match *t {
            Token::Keyword(KeywordToken::True) | Token::Keyword(KeywordToken::False) |
            Token::Literal(_) | Token::Opening(DelimiterToken::Paren) |
            Token::Opening(DelimiterToken::Bracket) => true,
            _ => false
        }
    }

    /// Is a token a start of an expression
    fn may_start_expr(&self, t: &Token) -> bool {
        match *t {
            Token::ResSymbol("|") | Token::Opening(DelimiterToken::Brace) |
            Token::Keyword(KeywordToken::If) | Token::Keyword(KeywordToken::While) |
            Token::Keyword(KeywordToken::Let) | Token::Keyword(KeywordToken::For) |
            Token::Keyword(KeywordToken::Continue) | Token::Keyword(KeywordToken::Break) |
            Token::Keyword(KeywordToken::Return) | Token::Symbol(_) | Token::Ident(_) => true,
            _ => self.may_start_literal(t)
        }
    }

    fn parse_literal(&mut self) -> Result<ast::Literal> {
        self.expectations.push(Expectation::Literal);
        self.expectations.push(Expectation::Token(Token::Opening(DelimiterToken::Paren)));
        self.expectations.push(Expectation::Token(Token::Opening(DelimiterToken::Bracket)));
        match self.current.t {
            Token::Literal(LiteralToken::Str(s)) => {
                let span = self.current.span;
                let expanded = try!(self.expand_escapes(s, true, '"'));
                try!(self.bump());
                Ok(Spanned {
                    span: span,
                    t: ast::Literal_::Str(expanded)
                })
            }
            Token::Literal(LiteralToken::Char(c)) => {
                let span = self.current.span;
                let expanded = try!(self.expand_escapes(c, true, '\''));
                let mut chars = expanded.chars();
                if let Some(ch) = chars.next() {
                    if let Some(_) = chars.next() {
                        Err("invalid char literal".into())
                    } else {
                        try!(self.bump());
                        Ok(Spanned {
                            span: span,
                            t: ast::Literal_::Char(ch)
                        })
                    }
                } else {
                    Err("invalid char literal".into())
                }
            }
            Token::Literal(LiteralToken::Number(n)) => {
                let span = self.current.span;
                try!(self.bump());
                let n: String = n.chars().filter(|&c| c != '_').collect();
                let num = if n.starts_with("0x") {
                    u64::from_str_radix(&n[2..], 16)
                        .map_err(|e| self.ctx.diagnostic().span(span).emit(
                            &format!("not a valid base-16 number: {}", e)
                        ))
                } else if n.starts_with("0b") {
                    u64::from_str_radix(&n[2..], 2)
                        .map_err(|e| self.ctx.diagnostic().span(span).emit(
                            &format!("not a valid base-2 number: {}", e)
                        ))
                } else if n.starts_with("0o") {
                    u64::from_str_radix(&n[2..], 8)
                        .map_err(|e| self.ctx.diagnostic().span(span).emit(
                            &format!("not a valid base-8 number: {}", e)
                        ))
                } else if n.contains(|c| c == '.' || c == 'e' || c == 'E') {
                    // float, parsed during translation, because otherwise we could be losing
                    // precision and whatnot
                    return Ok(Spanned {
                        t: ast::Literal_::Float(intern::intern_string(n)),
                        span: span
                    });
                } else {
                    u64::from_str_radix(&*n, 10)
                        .map_err(|e| self.ctx.diagnostic().span(span).emit(
                            &format!("not a valid base-10 number: {}", e)
                        ))
                }.unwrap_or(0);
                Ok(Spanned {
                    t: ast::Literal_::Int(num),
                    span: span
                })
            }
            Token::Keyword(kt@KeywordToken::True) |
            Token::Keyword(kt@KeywordToken::False) => {
                let span = self.current.span;
                try!(self.bump());
                Ok(Spanned {
                    t: ast::Literal_::Bool(kt == KeywordToken::True),
                    span: span
                })
            }
            Token::Opening(DelimiterToken::Paren) => {
                // NB: this also parses parenthesized expressions
                let lo = self.current.span;
                try!(self.bump());
                let mut ts = Vec::new();
                loop {
                    // not checking for type of closing delimiter allows us to ensure they are
                    // balanced
                    if let Token::Closing(_) = self.current.t { break }
                    let t = try!(self.parse_expr());
                    ts.push(t);
                    try!(self.optional_space());
                    if let Token::Closing(_) = self.current.t { break }
                    try!(eat_token!(self, Token::ResSymbol(",")));
                    try!(self.optional_space());
                }
                let hi = self.current.span;
                try!(eat_token!(self, Token::Closing(DelimiterToken::Paren)));
                Ok(self.mk_span(lo, hi, ast::Literal_::Tuple(ts)))
            }
            Token::Opening(DelimiterToken::Bracket) => {
                let lo = self.current.span;
                try!(self.bump());
                let mut ts = Vec::new();
                loop {
                    // not checking for type of closing delimiter allows us to ensure they are
                    // balanced
                    if let Token::Closing(_) = self.current.t { break }
                    let t = try!(self.parse_expr());
                    ts.push(t);
                    try!(self.optional_space());
                    if let Token::Closing(_) = self.current.t { break }
                    try!(eat_token!(self, Token::ResSymbol(",")));
                    try!(self.optional_space());
                }
                let hi = self.current.span;
                try!(eat_token!(self, Token::Closing(DelimiterToken::Bracket)));
                Ok(self.mk_span(lo, hi, ast::Literal_::Array(ts)))
            }
            _ => self.unexpected()
        }
    }

    fn expand_escapes(&self, s: InternedStr, uni: bool, quot: char) -> Result<InternedStr> {
        let mut cow: Cow<'static, str> = s.into();
        let mut cow_indice = 0;
        let mut chars = s.char_indices();
        unsafe { loop {
            match chars.next() {
                Some((_, '\\')) => match chars.next() {
                    Some((_, '\\')) => {
                        let i = '\\'.encode_utf8(&mut cow.to_mut().as_mut_vec()[cow_indice..]);
                        cow_indice += i.unwrap();
                    }
                    Some((_, 'n')) => {
                        let i = '\n'.encode_utf8(&mut cow.to_mut().as_mut_vec()[cow_indice..]);
                        cow_indice += i.unwrap();
                    }
                    Some((_, 'r')) => {
                        let i = '\r'.encode_utf8(&mut cow.to_mut().as_mut_vec()[cow_indice..]);
                        cow_indice += i.unwrap();
                    }
                    Some((_, 't')) => {
                        let i = '\t'.encode_utf8(&mut cow.to_mut().as_mut_vec()[cow_indice..]);
                        cow_indice += i.unwrap();
                    }
                    Some((_, '0')) => {
                        let i = '\0'.encode_utf8(&mut cow.to_mut().as_mut_vec()[cow_indice..]);
                        cow_indice += i.unwrap();
                    }
                    Some((_, 'u')) if uni => {
                        if let Some((ifrom, '{')) = chars.next() {
                            let f = (&mut chars).take(6).find(|&(_, c)| c == '}');
                            if let Some((ito, _)) = f {
                                let i = try!(u32::from_str_radix(&s[ifrom+1..ito], 16)
                                                 .map_err(|_| "invalid unicode codepoint hex")
                                                 .and_then(|cp|
                                    char::from_u32(cp).ok_or("invalid unicode codepoint")
                                )).encode_utf8(&mut cow.to_mut().as_mut_vec()[cow_indice..]);
                                cow_indice += i.unwrap();
                            } else {
                                return Err("bad unicode escape, format is \\u{ABCDEF}".into());
                            }
                        } else {
                            return Err("bad unicode escape, format is \\u{ABCDEF}".into());
                        }
                    }
                    Some((_, c)) if c == quot => {
                        let i = quot.encode_utf8(&mut cow.to_mut().as_mut_vec()[cow_indice..]);
                        cow_indice += i.unwrap();
                    }
                    _ => {
                        return Err("incomplete or invalid escape".into());
                    }
                },
                Some((i, ch)) => {
                    let chlen = ch.len_utf8();
                    cow_indice = if i + chlen == cow_indice {
                        i
                    } else {
                        let i = ch.encode_utf8(&mut cow.to_mut().as_mut_vec()[cow_indice..]);
                        cow_indice + i.unwrap()
                    };
                }
                None => return Ok(match cow {
                    Cow::Borrowed(i) => i,
                    Cow::Owned(mut o) => {
                        o.as_mut_vec().truncate(cow_indice);
                        intern::intern_string(o)
                    }
                })
            }
        }}
    }


    /// Executes closure with a list of attributes parsed
    fn with_attributes<F, R>(&mut self, outer: bool, f: F) -> Result<R>
    where F: FnOnce(&mut Self, Vec<ast::Attribute>) -> Result<R> {
        let mut attrs = Vec::new();
        loop {
            try!(self.optional_space());
            let lo = self.current.span;
            self.expectations.push(Expectation::Token(Token::Attribute(!outer)));
            self.expectations.push(Expectation::Documentation(!outer));
            match self.current.t {
                Token::Documentation(t, _) => {
                    if t == outer { break }
                    let mut nodes = Vec::new();
                    let mut hi = self.current.span;
                    while let Token::Documentation(ty, tnode) = self.current.t {
                        if ty != t { break }
                        hi = self.current.span;
                        try!(self.bump());
                        nodes.push(tnode);
                    }
                    attrs.push(self.mk_span(lo, hi, ast::Attribute_::Doc(
                        ast::DocItem(false, nodes)
                    )));
                }
                Token::Attribute(t) => {
                    if t == outer { break }
                    try!(self.bump());
                    try!(eat_token!(self, Token::Opening(DelimiterToken::Bracket)));
                    try!(self.optional_space());
                    let n = try!(self.parse_ident());
                    let mut args = Vec::new();
                    loop {
                        self.expectations.push(Expectation::AttributeArgument);
                        if let Token::Literal(LiteralToken::Str(_)) = self.next.t {
                            try!(self.parse_space());
                            args.push(try!(self.parse_literal()));
                        } else {
                            break
                        }
                    }
                    try!(self.optional_space());
                    let hi = self.current.span;
                    try!(eat_token!(self, Token::Closing(DelimiterToken::Bracket)));
                    attrs.push(self.mk_span(lo, hi, ast::Attribute_::Regular {
                        name: n,
                        args: args
                    }));
                }
                _ => return f(self, attrs)
            }
        }
        f(self, attrs)
    }

    /// Parse an identifier
    fn parse_ident(&mut self) -> Result<ast::Name> {
        self.expectations.push(Expectation::Ident);
        if let Token::Ident(i) = self.current.t {
            let span = self.current.span;
            try!(self.bump());
            Ok(Spanned {
                span: span,
                t: ast::Name_::Str(i)
            })
        } else {
            self.unexpected()
        }
    }

    /// Parse a single infix identifier (symbol)
    fn parse_infix(&mut self) -> Result<ast::Name> {
        let lo = self.current.span;
        try!(eat_token!(self, Token::ResSymbol("`")));
        self.expectations.push(Expectation::Symbol);
        if let Token::Symbol(s) = self.current.t {
            try!(self.bump());
            let hi = self.current.span;
            try!(eat_token!(self, Token::ResSymbol("`")));
            Ok(self.mk_span(lo, hi, ast::Name_::Str(s)))
        } else {
            self.unexpected()
        }
    }

    /// Parses a composite type
    ///
    /// Only A → B is handled here. Every other type is parsed in parse_primitive_type.
    fn parse_type(&mut self) -> Result<ast::Type> {
        let lt = try!(self.parse_primitive_type());
        self.expectations.push(Expectation::Token(Token::ResSymbol("→")));
        match (&self.current.t, &self.next.t) {
            (&Token::ResSymbol("→"), _) => {
                try!(eat_token!(self, Token::ResSymbol("→")));
                let rt = try!(self.parse_type());
                Ok(self.mk_span(lt.span, rt.span, ast::Type_::To(Box::new(lt), Box::new(rt))))
            },
            (_, &Token::ResSymbol("→")) => {
                try!(self.optional_space());
                try!(eat_token!(self, Token::ResSymbol("→")));
                try!(self.optional_space());
                let rt = try!(self.parse_type());
                Ok(self.mk_span(lt.span, rt.span, ast::Type_::To(Box::new(lt), Box::new(rt))))
            },
            _ => Ok(lt)
        }
    }

    /// Parses a single primitive type
    ///
    /// Not composite type like `A -> B`, but A, [A], (A, B, …) etc
    fn parse_primitive_type(&mut self) -> Result<ast::Type> {
        self.expectations.push(Expectation::Ident);
        self.expectations.push(Expectation::Token(Token::Opening(DelimiterToken::Paren)));
        self.expectations.push(Expectation::Token(Token::Opening(DelimiterToken::Bracket)));
        match self.current.t {
            Token::Ident(_) => {
                let id = try!(self.parse_ident());
                Ok(Spanned {
                    span: id.span,
                    t: ast::Type_::Named(id)
                })
            }
            Token::Opening(DelimiterToken::Paren) => {
                let lo = self.current.span;
                try!(self.bump());
                try!(self.optional_space());
                if let Token::Closing(DelimiterToken::Paren) = self.current.t {
                    let hi = self.current.span;
                    try!(self.bump());
                    return Ok(self.mk_span(lo, hi, ast::Type_::Null));
                }
                let mut ts = Vec::new();
                loop {
                    let t = try!(self.parse_type());
                    ts.push(t);
                    try!(self.optional_space());
                    if let Token::Closing(DelimiterToken::Paren) = self.current.t {
                        break
                    }
                    try!(eat_token!(self, Token::ResSymbol(",")));
                    try!(self.optional_space());
                }
                let hi = self.current.span;
                try!(eat_token!(self, Token::Closing(DelimiterToken::Paren)));
                Ok(self.mk_span(lo,hi, ast::Type_::Tuple(ts)))
            }
            Token::Opening(DelimiterToken::Bracket) => {
                let lo = self.current.span;
                try!(self.bump());
                try!(self.optional_space());
                let inner = try!(self.parse_type());
                try!(self.optional_space());
                self.expectations.push(Expectation::Token(Token::ResSymbol(";")));
                if let Token::ResSymbol(";") = self.current.t {
                    try!(self.bump());
                    try!(self.optional_space());
                    self.expectations.push(Expectation::Number);
                    if let Token::Literal(LiteralToken::Number(_)) = self.current.t {
                        let size = try!(self.parse_literal());
                        let size = match size.t {
                            ast::Literal_::Int(s) => if s >= !0usize as u64 {
                                self.ctx.diagnostic().span(size.span).emit(
                                    "number is too big"
                                );
                                1
                            } else {
                                s as usize
                            },
                            _ => {
                                self.ctx.diagnostic().emit("array size must be an integer");
                                1
                            }
                        };
                        let hi = self.current.span;
                        try!(eat_token!(self, Token::Closing(DelimiterToken::Bracket)));
                        Ok(self.mk_span(lo, hi, ast::Type_::Array(Box::new(inner), size)))
                    } else {
                        self.unexpected()
                    }
                } else {
                    let hi = self.current.span;
                    try!(eat_token!(self, Token::Closing(DelimiterToken::Bracket)));
                    Ok(self.mk_span(lo, hi, ast::Type_::Slice(Box::new(inner))))
                }

            }
            _ => self.unexpected()
        }
    }

    fn parse_trait_types(&mut self) -> Result<Vec<ast::Type>> {
        let mut tys = Vec::new();
        tys.push(try!(self.parse_type()));
        loop {
            try!(self.optional_space());
            if let Token::ResSymbol(",") = self.current.t {
                try!(self.bump());
                try!(self.optional_space());
                tys.push(try!(self.parse_type()));
            } else {
                break
            }
        }
        Ok(tys)
    }

    /// Tells whether token `t` could be the start of a type
    fn is_like_type(&self, t: &Token) -> bool {
        match t {
            &Token::Ident(_) => true,
            &Token::Opening(DelimiterToken::Paren) => true,
            &Token::Opening(DelimiterToken::Bracket) => true,
            _ => false
        }
    }

    /// Parse a single space
    fn parse_space(&mut self) -> Result<()> {
        eat_token!(self, Token::Whitespace(SpaceKind::Space))
    }

    fn some_space(&mut self) -> Result<()> {
        self.expectations.push(Expectation::SomeWhitespace);
        eat_token!(self, ~Token::Whitespace(_))
    }

    /// Consumes all whitespace tokens until the next non-whitespace token.
    fn optional_space(&mut self) -> Result<()> {
        while let Token::Whitespace(_) = self.current.t {
            try!(self.bump_tokens());
        }
        Ok(())
    }

    /// Produce an unexpected token error.
    ///
    /// The error will contain the token found and the tokens expected (provided self.expected was
    /// filled in correctly)
    fn unexpected<T>(&mut self) -> Result<T> {
        self.ctx.diagnostic().span(self.current.span).emit(&*if self.expectations.len() == 0 {
            format!("unexpected token: found {}", self.current.t)
        } else {
            let mut s = format!("unexpected token: found {}\n\texpected: ", self.current.t);
            while let Some(e) = self.expectations.pop() {
                // deduplicate
                while let Some(p) = self.expectations.iter().position(|e2| *e2 == e) {
                    self.expectations.swap_remove(p);
                }

                fn w<T: ::std::fmt::Display>(s: &mut String, t: T, l: usize) {
                    write!(s, "{}{}", t, if l == 1 { " or " }
                        else if l > 1 { ", " }
                        else { "" }
                    ).unwrap();
                }

                let len = self.expectations.len();
                match e {
                    Expectation::Token(t) => w(&mut s, t, len),
                    Expectation::Ident => w(&mut s, "ident", len),
                    Expectation::SomeWhitespace => w(&mut s, "some whitespace", len),
                    Expectation::Number => w(&mut s, "number", len),
                    Expectation::Literal => w(&mut s, "literal", len),
                    Expectation::AttributeArgument => w(&mut s, "attribute argument", len),
                    Expectation::Symbol => w(&mut s, "symbol", len),
                    Expectation::Item => w(&mut s, "item", len),
                    Expectation::Documentation(true) => w(&mut s, "inner documentation", len),
                    Expectation::Documentation(false) => w(&mut s, "outer documentation", len),
                }
            }
            s
        });
        Err(Error::ParserError(ParserErrorKind::UnexpectedToken))
    }

    fn mk_span<T>(&mut self, lo: SpanId, hi: SpanId, el: T) -> Spanned<T> {
        let losid = { self.ctx.span(lo).sid };
        let hisid = { self.ctx.span(lo).sid };
        assert!(losid == hisid, "low span source id and high span source id do not match");
        let lo = { self.ctx.span(lo).lo };
        let hi = { self.ctx.span(hi).hi };
        let sp = self.ctx.new_span(losid, lo, hi);
        Spanned {
            t: el,
            span: sp
        }
    }

    /// Parses an expression from callexpr token tree. Must have information about operator fixity.
    fn parse_from_callexpr(ctx: &mut context::Context, mut list: I)
    -> Result<ast::Expr> {
        let current = try!(LexerIterator::next(&mut list, ctx));
        let next = try!(LexerIterator::next(&mut list, ctx));
        let mut parser = Parser {
            ctx: ctx,
            lexer: list,
            current: current,
            next: next,
            expectations: Vec::new(),
            token_collector: None,
        };
        parser.parse_assoc_expr(0)
    }

    fn parse_assoc_expr(&mut self, min_prec: u8) -> Result<ast::Expr> {
        let mut lhs = try!(self.parse_assoc_component());
        loop {
            match (self.current.t, self.next.t) {
                (Token::Whitespace(SpaceKind::Space), Token::EOF) |
                (Token::Whitespace(SpaceKind::Space), Token::EOE) |
                (Token::EOF, _) | (Token::EOE, _) | (Token::ResSymbol("|"), _) |
                (Token::ResSymbol("||"), _) | (Token::Opening(DelimiterToken::Brace), _) |
                (Token::Ident(_), _) => break,

                (Token::Whitespace(_), Token::ResSymbol(s)) |
                (Token::ResSymbol(s), _) |
                (Token::Whitespace(_), Token::Symbol(s)) |
                (Token::Symbol(s), _) => {
                    try!(self.optional_space());
                    let op_span = self.current.span;
                    let op = self.ctx.operators.get_operator(s).map_err(|_| {
                        self.ctx.diagnostic().span(op_span)
                            .emit(&*format!("operator {} is not defined", s));
                        Error::Quiet
                    // Use a default operator in case it does not exist to continue parsing and
                    // possibly finding more errors.
                    }).unwrap_or(ops::Operator {
                        symbol: s,
                        precedence: 10,
                        fixity: Fixity::Left
                    });
                    if op.precedence < min_prec { break }
                    try!(self.bump());
                    let rhs = try!(match op.fixity {
                        Fixity::Left => {
                            try!(self.optional_space());
                            self.parse_assoc_expr(op.precedence)
                        }
                        Fixity::Right => {
                            try!(self.optional_space());
                            self.parse_assoc_expr(op.precedence + 1)
                        }
                        Fixity::None => {
                            try!(self.optional_space());
                            self.parse_assoc_expr(op.precedence + 1)
                        }
                    });
                    lhs = if s == "=" {
                        self.mk_span(lhs.span, rhs.span, ast::Expr_::Assign(
                            Box::new(lhs), Box::new(rhs)
                        ))
                    } else {
                        self.mk_span(lhs.span, rhs.span, ast::Expr_::CallTree(
                            Box::new(Spanned {
                                t: ast::Expr_::Ident(Spanned {
                                    t: ast::Name_::Str(op.symbol),
                                    span: op_span
                                }),
                                span: op_span
                            }),
                            vec![lhs, rhs]
                        ))
                    };
                    if let Fixity::None = op.fixity { break }
                },
                (Token::Whitespace(SpaceKind::Space), _) => {
                    let mut args = Vec::new();
                    let mut hi = self.current.span;
                    while let Token::Whitespace(SpaceKind::Space) = self.current.t {
                        try!(self.parse_space());
                        let comp = try!(self.parse_assoc_component());
                        hi = comp.span;
                        args.push(comp);
                    }
                    lhs = self.mk_span(lhs.span, hi, ast::Expr_::CallTree(
                        Box::new(lhs),
                        args
                    ));
                },
                (l, _) => if self.may_start_literal(&l) {
                    break
                } else {
                    return self.unexpected();
                }
            }
        }
        Ok(lhs)
    }

    fn parse_assoc_component(&mut self) -> Result<ast::Expr> {
        // self.expectations.push(Expectation::Symbol);
        self.expectations.push(Expectation::Ident);
        self.expectations.push(Expectation::Token(Token::Opening(DelimiterToken::Brace)));
        self.expectations.push(Expectation::Token(Token::ResSymbol("|")));
        self.expectations.push(Expectation::Literal);
        match self.current.t {
            Token::Opening(DelimiterToken::Brace) => self.parse_block_expr(),
            Token::ResSymbol("|") => self.parse_lambda_expr(),
            Token::Ident(_) => {
                let i = try!(self.parse_ident());
                Ok(Spanned {
                    span: i.span,
                    t: ast::Expr_::Ident(i),
                })
            }
            _ => if self.may_start_literal(&self.current.t) {
                let l = try!(self.parse_literal());
                Ok(Spanned {
                    span: l.span,
                    t: ast::Expr_::Literal(l),
                })
            } else {
                self.unexpected()
            }
        }
   }

}
