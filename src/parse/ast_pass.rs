/// AST passes walk the AST potentially modifying it to a different AST.

use super::ast;
use context;
use intern;
use error::{Result, Error};
use parse::{self, ops};


/// ASTPass trait defines interface for a walker. All methods have a default implementation which
/// does not change the AST in any way.
pub trait ASTPass {
    fn walk_program(&mut self, p: &mut ast::Program) -> Result<()> {
        try!(self.insp_program(p));
        p.items.iter_mut().fold(Ok(()), |r, x| r.and_then(|_| self.walk_item(x)))
    }
    fn walk_item(&mut self, p: &mut ast::Item) -> Result<()> {
        try!(self.insp_item(p));
        match *p {
            ast::Item::Type(_) | ast::Item::Enum(_) => Ok(()),
            ast::Item::Trait(ref mut t) =>
                t.lets.iter_mut().fold(Ok(()), |r, l| r.and_then(|_| self.walk_let_item(l))),
            ast::Item::Impl(ref mut t) =>
                t.lets.iter_mut().fold(Ok(()), |r, l| r.and_then(|_| self.walk_let_item(l))),
            ast::Item::Let(ref mut t) => self.walk_let_item(t),
        }
    }

    fn walk_let_item(&mut self, p: &mut ast::LetItem) -> Result<()> {
        try!(self.insp_let_item(p));
        if let Some(s) = p.t.init.as_mut() {
            return self.walk_stmt(s);
        }
        Ok(())
    }

    fn walk_stmt(&mut self, p: &mut ast::Stmt)  -> Result<()>{
        try!(self.insp_stmt(p));
        match &mut p.t {
            &mut ast::Stmt_::Let(ref mut i) => self.walk_let_item(&mut *i),
            &mut ast::Stmt_::Expr(ref mut i) => self.walk_expr(&mut *i)
        }
    }

    fn walk_expr(&mut self, p: &mut ast::Expr) -> Result<()> {
        try!(self.insp_expr(p));
        match &mut p.t {
            &mut ast::Expr_::Block(ref mut v, ref mut ex) => {
                try!(v.iter_mut().fold(Ok(()), |r, s| r.and_then(|_| self.walk_stmt(s))));
                if let Some(e) = ex.as_mut() {
                    return self.walk_expr(&mut *e);
                }
                Ok(())
            },
            &mut ast::Expr_::Lambda(_, ref mut ex) => {
                self.walk_expr(&mut *ex)
            },
            &mut ast::Expr_::If { cond: ref mut c, body: ref mut b, elbr: ref mut br } => {
                try!(self.walk_expr(&mut *c));
                try!(self.walk_expr(&mut *b));
                if let Some(br) = br.as_mut() {
                    return self.walk_expr(&mut *br);
                }
                Ok(())
            },
            &mut ast::Expr_::While { cond: ref mut c, body: ref mut b } => {
                try!(self.walk_expr(&mut *c));
                self.walk_expr(&mut *b)
            },
            &mut ast::Expr_::For { bind: _, iter: ref mut i, body: ref mut b } => {
                try!(self.walk_expr(&mut *i));
                self.walk_expr(&mut *b)
            },
            &mut ast::Expr_::Return(ref mut e) => {
                self.walk_expr(&mut *e)
            },
            &mut ast::Expr_::Assign(ref mut e1, ref mut e2) => {
                try!(self.walk_expr(&mut *e1));
                self.walk_expr(&mut *e2)
            },
            &mut ast::Expr_::CallTree(ref mut expr, ref mut exprs) => {
                try!(self.walk_expr(expr));
                for expr in exprs {
                    try!(self.walk_expr(expr));
                }
                Ok(())
            }
            &mut ast::Expr_::Literal(ref mut l) => {
                try!(self.insp_literal(l));
                match l.t {
                    ast::Literal_::Tuple(ref mut exprs) => {
                        for expr in exprs {
                            try!(self.walk_expr(expr));
                        }
                        Ok(())
                    },
                    ast::Literal_::Array(ref mut exprs) => {
                        for expr in exprs {
                            try!(self.walk_expr(expr));
                        }
                        Ok(())
                    },
                    _ => Ok(())
                }
            }
            &mut ast::Expr_::CallExpr(_) => Ok(()),
            &mut ast::Expr_::Continue => return Ok(()),
            &mut ast::Expr_::Break => return Ok(()),
            &mut ast::Expr_::Ident(_) => return Ok(()),
        }
    }

    fn insp_program(&mut self, _: &mut ast::Program) -> Result<()> {
        Ok(())
    }
    fn insp_item(&mut self, _: &mut ast::Item)  -> Result<()>{
        Ok(())
    }
    fn insp_let_item(&mut self, _: &mut ast::LetItem) -> Result<()> {
        Ok(())
    }
    fn insp_stmt(&mut self, _: &mut ast::Stmt) -> Result<()> {
        Ok(())
    }
    fn insp_expr(&mut self, _: &mut ast::Expr) -> Result<()> {
        Ok(())
    }
    fn insp_literal(&mut self, _: &mut ast::Literal) -> Result<()> {
        Ok(())
    }
}



pub struct FixityCollectPass<'c> {
    c: &'c mut context::Context,
}

impl<'c> FixityCollectPass<'c> {
    pub fn new(c: &'c mut context::Context) -> FixityCollectPass {
        FixityCollectPass { c: c }
    }

    fn fixity_attr(&mut self, i: &ast::LetItem) -> Option<(ops::Fixity, u8)> {
        // TODO inference failure here if annotation is removed
        let mut a = None::<&context::Spanned<_>>;
        let mut ret = None;
        for attr in i.attributes.iter() {
            if attr.check_name("fixity") {
                if a.is_some() {
                    self.c.diagnostic().warning().span(attr.span)
                        .emit("multiple fixity attributes found");
                    self.c.diagnostic().hint().span(a.unwrap().span)
                        .emit("previous fixity attribute");
                }
                a = Some(attr);
                if let ast::Attribute_::Regular { ref args, .. } = attr.t {
                    if args.len() != 2 {
                        self.c.diagnostic().span(attr.span).emit("malformed fixity attribute");
                    } else {
                        let fixity = match attr.get_arg(0).unwrap() {
                            "left" => ops::Fixity::Left,
                            "right" => ops::Fixity::Right,
                            "none" => ops::Fixity::None,
                            s => {
                                self.c.diagnostic().error().span(args[0].span)
                                    .emit(&*format!("expected left, right or none, found {}", s));
                                ops::Fixity::None
                            }
                        };
                        let prec = match attr.get_arg(1).unwrap().parse() {
                            Ok(n) => n,
                            Err(e) => {
                                self.c.diagnostic().error().span(args[1].span)
                                    .emit(&*format!("precedence is malformed: {}", e));
                                255
                            }
                        };
                        ret = Some((fixity, prec))
                    }
                }
            }
        }
        ret
    }
}

impl<'c> ASTPass for FixityCollectPass<'c> {
    fn insp_let_item(&mut self, p: &mut ast::LetItem) -> Result<()> {
        if let Some((fixity, precedence)) = self.fixity_attr(p) {
            if let ast::LetName::Regular(_) = p.name {
                self.c.diagnostic().warning().span(p.span)
                    .emit("non-infix item with fixity attribute");
            }
            let ident: intern::InternedStr = if let ast::Name_::Str(n) = p.name.as_ref().t {
                n
            } else { panic!() };
            if let Err(e) = self.c.operators.add_operator(ident, fixity, precedence) {
                self.c.diagnostic().span(p.span)
                    .emit(&*format!("{}", e));
                return Err(Error::Quiet)
            }
            Ok(())
        } else {
            Ok(())
        }
    }
}


pub struct ResolveUnparsedPass<'c> {
    c: &'c mut context::Context,
}

impl<'c> ResolveUnparsedPass<'c> {
    pub fn new(c: &mut context::Context) -> ResolveUnparsedPass {
        ResolveUnparsedPass {
            c: c
        }
    }
}

impl<'c> ASTPass for ResolveUnparsedPass<'c> {
    fn insp_expr(&mut self, expr: &mut ast::Expr) -> Result<()> {
        let parsed: Option<ast::Expr> = if let ast::Expr_::CallExpr(ref mut vec) = expr.t {
            Some(try!(parse::Parser::parse_from_callexpr(self.c, vec.iter())))
        } else {
            None
        };
        parsed.map(|t| ::std::mem::replace(expr, t));
        Ok(())
    }
}
