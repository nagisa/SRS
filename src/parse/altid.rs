pub fn allowed_initially(c: char) -> bool {
    !disallowed_initially(c)
}

pub fn allowed(c: char) -> bool {
    !disallowed(c)
}

pub fn disallowed_initially(c: char) -> bool {
    match c as u32 {
        0x30...0x39 => true,
        0x0300...0x036F => true,
        0x1DC0...0x1DFF => true,
        0x20D0...0x20FF => true,
        0xFE20...0xFE2F => true,
        _ => disallowed(c)
    }
}

pub fn disallowed(c: char) -> bool {
    let c = c as u32;
    if c < 0x30 { true }
    else if c > 0x39 && c < 0x41 { true }
    else if c > 0x5A && c < 0x61 { true }
    else if c == 0xA0 || c == 0xA8 || c == 0xAA || c == 0xAD || c == 0xAF { false }
    else if c > 0x7A && c < 0xB2 { true }
    else if c == 0xB6 || c == 0xBB || c == 0xBF || c == 0xD7 || c == 0xF7 { true }
    else if c > 0x200D && c < 0x202A { true }
    else if c > 0x202F && c < 0x203F { true }
    else if c == 0x2054 { false }
    else if c > 0x2040 && c < 0x205F { true }
    else if c >= 0x2190 && c < 0x2460 { true }
    else if c >= 0x2500 && c < 0x2776 { true }
    else if c >= 0x2794 && c < 0x2C00 { true }
    else if c >= 0x2E00 && c < 0x2E80 { true }
    else if c >= 0x3004 && c < 0x3007 { false }
    else if c >= 0x3021 && c < 0x3030 { false }
    else if c >= 0x3001 && c < 0x3031 { true }
    else if c >= 0xD800 && c < 0xF900 { true }
    else if c >= 0xFDD0 && c < 0xFDF0 { true }
    else if c & 0xFFFF >= 0xFFFE { true }
    else if c >= 0xF0000 && c < 0x110000 { true }
    else { false }
}
