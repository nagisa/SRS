use intern::InternedStr;
use super::lex;
use resolve;
pub use context::Spanned as Spanned;

#[derive(Debug)]
pub struct Program {
    pub items: Vec<Item>,
    pub attributes: Vec<Attribute>
}

#[derive(Debug)]
pub enum Item {
    Type(TypeItem),
    Enum(EnumItem),
    Trait(TraitItem),
    Impl(ImplItem),
    Let(LetItem)
}

pub type TypeItem = Spanned<TypeItem_>;

#[derive(Debug)]
pub struct TypeItem_ {
    pub name: Name,
    pub fields: Vec<TypeItemField>,
    pub attributes: Vec<Attribute>
}

pub type TypeItemField = Spanned<TypeItemField_>;

#[derive(Debug)]
pub struct TypeItemField_ {
    pub attributes: Vec<Attribute>,
    pub name: Name,
    pub ty: Type
}

pub type EnumItem = Spanned<EnumItem_>;

#[derive(Debug)]
pub struct EnumItem_ {
    pub name: Name,
    pub variants: Vec<EnumItemVariant>,
    pub attributes: Vec<Attribute>
}

pub type EnumItemVariant = Spanned<EnumItemVariant_>;

#[derive(Debug)]
pub struct EnumItemVariant_ {
    pub attributes: Vec<Attribute>,
    pub name: Name,
    pub contained_ty: Option<Type>
}

pub type LetItem = Spanned<LetItem_>;

#[derive(Debug)]
pub struct LetItem_ {
    pub attributes: Vec<Attribute>,
    pub name: LetName,
    pub mutable: bool,
    pub ty: Type,
    pub init: Option<Stmt>
}

pub type TraitItem = Spanned<TraitItem_>;

#[derive(Debug)]
pub struct TraitItem_ {
    pub attributes: Vec<Attribute>,
    pub name: Name,
    pub tys: Vec<Type>,
    pub lets: Vec<LetItem>
}

pub type ImplItem = Spanned<ImplItem_>;

#[derive(Debug)]
pub struct ImplItem_ {
    pub attributes: Vec<Attribute>,
    pub name: Name,
    pub tys: Vec<Type>,
    pub lets: Vec<LetItem>
}

#[derive(Debug)]
pub struct DocItem(pub bool, pub Vec<InternedStr>);

pub type Type = Spanned<Type_>;

#[derive(Debug)]
pub enum Type_ {
    Null, // Empty tuple
    Tuple(Vec<Type>),
    Array(Box<Type>, usize),
    Slice(Box<Type>),
    Named(Name),
    To(Box<Type>, Box<Type>)
}

pub type Attribute = Spanned<Attribute_>;

#[derive(Debug)]
pub enum Attribute_{
    Doc(DocItem),
    Regular {
        name: Name,
        args: Vec<Literal>
    }
}

impl Attribute_ {
    pub fn check_name(&self, name: &str) -> bool {
        if let Attribute_::Regular { name: ref n, .. } = *self {
            if let Name_::Str(n) = n.t {
                n == name
            } else {
                false
            }
        } else {
            false
        }
    }

    pub fn get_arg(&self, idx: usize) -> Option<InternedStr> {
        if let &Attribute_::Regular { ref args, .. } = self {
            if let Literal_::Str(s) = args[idx].t {
                Some(s)
            } else {
                panic!("attribute contains non-str literals")
            }
        } else {
            None
        }
    }
}

pub type Stmt = Spanned<Stmt_>;

#[derive(Debug)]
pub enum Stmt_ {
    Let(Box<LetItem>),
    Expr(Box<Expr>)
}

pub type Expr = Spanned<Expr_>;

#[derive(Debug)]
pub enum Expr_ {
    Block(Vec<Stmt>, Option<Box<Expr>>),
    Lambda(Vec<Name>, Box<Expr>),
    If {
        cond: Box<Expr>,
        body: Box<Expr>,
        elbr: Option<Box<Expr>>
    },
    While {
        cond: Box<Expr>,
        body: Box<Expr>
    },
    For {
        bind: Name,
        iter: Box<Expr>,
        body: Box<Expr>,
    },
    Continue,
    Break,
    Return(Box<Expr>),
    Ident(Name),
    Literal(Literal),
    Assign(Box<Expr>, Box<Expr>),
    CallTree(Box<Expr>, Vec<Expr>),
    /// A yet unparsed list of tokens (this is the state of most/all expressions before operator
    /// fixity is figured out. Once all operator fixities are figured, this gets replaced by the
    /// Parsed variant.
    CallExpr(Vec<Spanned<lex::Token>>),
}

pub type Literal = Spanned<Literal_>;

#[derive(Debug)]
pub enum Literal_ {
    Str(InternedStr),
    Char(char),
    Int(u64),
    Float(InternedStr), // Floats are converted into machine repr during trans
    Bool(bool),
    Tuple(Vec<Expr>),
    Array(Vec<Expr>)
}

// utilities --
pub enum StmtExpr {
    Stmt(Stmt),
    Expr(Expr)
}

#[derive(Debug)]
pub enum LetName {
    Infix(Name),
    Regular(Name)
}

impl AsRef<Name> for LetName {
    fn as_ref(&self) -> &Name {
        match *self {
            LetName::Infix(ref i) => i,
            LetName::Regular(ref i) => i
        }
    }
}

impl AsMut<Name> for LetName {
    fn as_mut(&mut self) -> &mut Name {
        match *self {
            LetName::Infix(ref mut i) => i,
            LetName::Regular(ref mut i) => i
        }
    }
}

pub type Name = Spanned<Name_>;

// No Copy or Clone please
#[derive(Debug, PartialEq, Eq)]
pub enum Name_ {
    Str(InternedStr),
    Name(resolve::Name),
}

impl Name_ {
    /// Converts this ident to resolved Name
    pub fn make_name(&mut self, n: resolve::Name) {
        ::std::mem::replace(self, Name_::Name(n));
    }
}
