use super::altid::{allowed, allowed_initially};
use error::{Result, LexerErrorKind, Error};
use context;
use std::str;
use intern::{InternedStr, intern};
use std::fmt;


#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Token {
    // These contain the comment text as well.
    Keyword(KeywordToken),
    Ident(InternedStr),
    Literal(LiteralToken),

    Symbol(InternedStr),
    ResSymbol(InternedStr),
    Opening(DelimiterToken),
    Closing(DelimiterToken),

    Attribute(bool), // refers to # or #!, aka shebang
    Whitespace(SpaceKind),
    Comment(InternedStr),
    Documentation(bool, InternedStr),
    EOF,
    EOE
}

impl fmt::Display for Token {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &Token::Keyword(t) => fmt::Display::fmt(&t, f),
            &Token::Ident(t) => fmt::Display::fmt(t, f),
            &Token::Literal(t) => fmt::Display::fmt(&t, f),
            &Token::Symbol(t) => fmt::Display::fmt(t, f),
            &Token::ResSymbol(t) => fmt::Display::fmt(t, f),
            &Token::Opening(DelimiterToken::Paren) => fmt::Display::fmt("(", f),
            &Token::Opening(DelimiterToken::Brace) => fmt::Display::fmt("{", f),
            &Token::Opening(DelimiterToken::Bracket) => fmt::Display::fmt("[", f),
            &Token::Closing(DelimiterToken::Paren) => fmt::Display::fmt(")", f),
            &Token::Closing(DelimiterToken::Brace) => fmt::Display::fmt("}", f),
            &Token::Closing(DelimiterToken::Bracket) => fmt::Display::fmt("]", f),
            &Token::Attribute(true) => fmt::Display::fmt("#!", f),
            &Token::Attribute(false) => fmt::Display::fmt("#", f),
            &Token::Comment(_) => fmt::Display::fmt("comment", f),
            &Token::Whitespace(s) => fmt::Display::fmt(&s, f),
            &Token::Documentation(true, _) => fmt::Display::fmt("inner documentation", f),
            &Token::Documentation(false, _) => fmt::Display::fmt("outer documentation", f),
            &Token::EOF => fmt::Display::fmt("end of file", f),
            &Token::EOE => fmt::Display::fmt("end of expression", f),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum SpaceKind {
    Space,
    Other
}

impl fmt::Display for SpaceKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &SpaceKind::Space => fmt::Display::fmt("space", f),
            &SpaceKind::Other => fmt::Display::fmt("other whitespace", f)
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum DelimiterToken {
    /// `(` or `)`
    Paren,
    /// `[` or `]`
    Bracket,
    /// `{` or `}`
    Brace
}

impl DelimiterToken {
    fn from_char(c: char) -> DelimiterToken {
        match c {
            '(' | ')' => DelimiterToken::Paren,
            '[' | ']' => DelimiterToken::Bracket,
            '{' | '}' => DelimiterToken::Brace,
            _ => panic!("DelimiterToken::from_char with unsupported char")
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum KeywordToken {
    As,
    Break,
    Continue,
    Else,
    Enum,
    Extern,
    False,
    For,
    If,
    Impl,
    In,
    Let,
    Match,
    Mut,
    Return,
    Self_,
    Struct,
    Super,
    Trait,
    True,
    Type,
    While
}

use self::KeywordToken::*;
impl KeywordToken {
    const PAIRS: [(&'static str, KeywordToken); 22] = [
        ("as", As), ("break", Break), ("continue", Continue), ("else", Else), ("enum", Enum),
        ("extern", Extern), ("false", False), ("for", For), ("if", If), ("impl", Impl), ("in", In),
        ("let", Let), ("match", Match), ("mut", Mut), ("return", Return), ("Self", Self_),
        ("struct", Struct), ("super", Super), ("trait", Trait), ("true", True), ("type", Type),
        ("while", While)
    ];

    pub fn from_str(s: &str) -> Option<KeywordToken> {
        KeywordToken::PAIRS.iter().find(|&&(ts, _)| ts == s).map(|&(_, t)| t)
    }

    pub fn best_from_str(s: &str) -> Option<(usize, KeywordToken)> {
        KeywordToken::PAIRS.iter().map(|&(ts, t)|
           (KeywordToken::levenshtein_distance(ts, s), t)
        ).min_by(|&(ts, _)| ts)
    }

    fn levenshtein_distance(word1: &str, word2: &str) -> usize {
        let word1_length = word1.len() + 1;
        let word2_length = word2.len() + 1;
        let mut matrix = vec![vec![0]];
        for i in 1..word1_length { matrix[0].push(i); }
        for j in 1..word2_length { matrix.push(vec![j]); }
        for j in 1..word2_length {
            for i in 1..word1_length {
                let x: usize = if word1.chars().nth(i - 1) == word2.chars().nth(j - 1) {
                    matrix[j-1][i-1]
                } else {
                    let min_distance = [matrix[j][i-1], matrix[j-1][i], matrix[j-1][i-1]];
                    *min_distance.iter().min().unwrap() + 1
                };
                matrix[j].push(x);
            }
        }
        matrix[word2_length-1][word1_length-1]
    }
}

impl fmt::Display for KeywordToken {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &KeywordToken::As => fmt::Display::fmt("as", f),
            &KeywordToken::Break => fmt::Display::fmt("break", f),
            &KeywordToken::Continue => fmt::Display::fmt("continue", f),
            &KeywordToken::Else => fmt::Display::fmt("else", f),
            &KeywordToken::Enum => fmt::Display::fmt("enum", f),
            &KeywordToken::Extern => fmt::Display::fmt("extern", f),
            &KeywordToken::False => fmt::Display::fmt("false", f),
            &KeywordToken::For => fmt::Display::fmt("for", f),
            &KeywordToken::If => fmt::Display::fmt("if", f),
            &KeywordToken::Impl => fmt::Display::fmt("impl", f),
            &KeywordToken::In => fmt::Display::fmt("in", f),
            &KeywordToken::Let => fmt::Display::fmt("let", f),
            &KeywordToken::Match => fmt::Display::fmt("match", f),
            &KeywordToken::Mut => fmt::Display::fmt("mut", f),
            &KeywordToken::Return => fmt::Display::fmt("return", f),
            &KeywordToken::Self_ => fmt::Display::fmt("Self", f),
            &KeywordToken::Struct => fmt::Display::fmt("struct", f),
            &KeywordToken::Super => fmt::Display::fmt("super", f),
            &KeywordToken::Trait => fmt::Display::fmt("trait", f),
            &KeywordToken::True => fmt::Display::fmt("true", f),
            &KeywordToken::Type => fmt::Display::fmt("type", f),
            &KeywordToken::While => fmt::Display::fmt("while", f),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum LiteralToken {
    Str(InternedStr),
    Char(InternedStr),
    Number(InternedStr),
}

impl fmt::Display for LiteralToken {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &LiteralToken::Str(s) => write!(f, "\"{}\"", s),
            &LiteralToken::Char(c) => write!(f, "'{}'", c),
            &LiteralToken::Number(n) => fmt::Display::fmt(n, f),
        }
    }
}

pub struct Lexer {
    sid: context::SourceId,
    source_start: InternedStr,
    source_curr: InternedStr
}


impl Lexer {
    pub fn new(s: &context::Context, sid: context::SourceId) -> Result<Lexer> {
        let start = intern(try!(s.source(sid).text()));
        Ok(Lexer {
            sid: sid,
            source_start: start,
            source_curr: start
        })
    }

    fn try<R, T: FnOnce(&mut Lexer) -> Option<R>>(&mut self, f: T) -> Result<R> {
        let old = self.source_curr;
        let r = f(self);
        match r {
            None => {
                self.source_curr = old;
                Err(Error::LexerError(LexerErrorKind::NotToken))
            }
            Some(x) => Ok(x)
        }
    }

    fn advance(&mut self, num: usize) {
        self.source_curr = &self.source_curr[num..];
    }

    fn eat(&mut self, prefix: &str) -> Option<()> {
        if self.source_curr.starts_with(prefix) {
            self.advance(prefix.len());
            Some(())
        } else {
            None
        }
    }

    fn lex_comment(&mut self) -> Option<Token> {
        self.eat("//").and_then(|_| {
            let len = self.source_curr.find('\n')
                .map(|x| x + 1)
                .unwrap_or_else(|| self.source_curr.len());
            let txt = &self.source_curr[..len];
            self.advance(len);
            Some(Token::Comment(txt))
        })
    }

    fn lex_documentation(&mut self) -> Option<Token> {
        self.eat("//!").map(|_| true)
            .or_else(|| self.eat("///").map(|_| false))
            .and_then(|is_inner| {
                let len = self.source_curr.find('\n')
                    .map(|x| x + 1)
                    .unwrap_or_else(|| self.source_curr.len());
                let txt = &self.source_curr[..len];
                self.advance(len);
                Some(Token::Documentation(is_inner, txt))
            })
    }

    fn lex_attribute(&mut self) -> Option<Token> {
        self.eat("#!").map(|_| true)
            .or_else(|| self.eat("#").map(|_| false))
            .map(|is_inner| Token::Attribute(is_inner))
    }

    fn eat_to_string_end(&mut self, quot: char) -> Option<InternedStr> {
        AfterCharIndices::new(self.source_curr)
            .scan((0, '\0', '\0'), |o, n| {
                *o = (n.0, o.2, n.1);
                Some(*o)
            })
            .find(|x| x.1 != '\\' && x.2 == quot)
            .map(|x| {
                let i = x.0;
                let t = &self.source_curr[..i-1];
                self.advance(i);
                t
            })
    }

    fn lex_string(&mut self) -> Option<LiteralToken> {
        self.eat("\"")
            .and_then(|_| {
                self.eat_to_string_end('"').map(LiteralToken::Str)
            })
    }

    fn lex_char(&mut self) -> Option<LiteralToken> {
        self.eat("'")
            .and_then(|_| {
                self.eat_to_string_end('\'').map(LiteralToken::Char)
            })
    }

    fn lex_number(&mut self) -> Option<LiteralToken> {
        self.source_curr.chars().next().and_then(|d| {
            if let '0'...'9' = d {
                let number = AfterCharIndices::new(self.source_curr)
                    .take_while(|x|{ // Only take allowed characters
                        match x.1 {
                            '0'...'9' => true, // decimal digits
                            'A'...'F' => true, // hex digits + exponent
                            'a'...'f' => true, // hex digits + binary specifier + exponent
                            'o' | 'x' => true, // octal and hex specifiers
                            '_' => true,       // convenience separator (e.g. 0xFFFF_FFFF)
                            '.' => true,       // floating part
                            _ => false
                        }
                    }).last();
                number.map(|x| {
                    let ret = &self.source_curr[..x.0];
                    self.advance(x.0);
                    LiteralToken::Number(ret)
                })
            } else {
                None
            }
        })
    }

    fn lex_literal(&mut self) -> Option<Token> {
        self.try(Lexer::lex_string)
             .or_else(|_|
                self.try(Lexer::lex_char)
            ).or_else(|_|
                self.try(Lexer::lex_number)
            ).ok().map(|x| Token::Literal(x))
    }

    fn lex_symbol(&mut self) -> Option<Token> {
        for &(sym, chr) in [("(", '('), ("{", '{'), ("[", '[')].into_iter() {
            if self.eat(sym).is_some() {
                return Some(Token::Opening(DelimiterToken::from_char(chr)));
            }
        }
        for &(sym, chr) in [(")", ')'), ("}", '}'), ("]", ']')].into_iter() {
            if self.eat(sym).is_some() {
                return Some(Token::Closing(DelimiterToken::from_char(chr)));
            }
        }

        let reserved_syms = [":", ";", ",", "|", "→", "←", "`"];
        // NOTE: there’s a special symbol (=) that’s not in this list. See note below for reasoning
        for &reserved in reserved_syms.into_iter() {
            if self.eat(reserved).is_some() {
                return Some(Token::ResSymbol(reserved));
            }
        }

        // FIXME: should be characters that have unicode symbol property
        let syms = ["=", "\\", "_", "!", "$", "%", "&", "*", "+", "-", ".", "/", "?", "@", "^", "~",
                    "<", ">"];
        AfterCharIndices::new(self.source_curr).take_while(|&(_, c)|
            syms.iter().any(|c1| c1.contains(c))
        ).last().map(|(i, _)| {
            let sym = &self.source_curr[..i];
            self.advance(i);
            // there’s a special symbol (`=`) here, that’s a reserved symbol, but only when it is
            // by itself. Compare and decide here.
            if sym == "=" {
                Token::ResSymbol(sym)
            } else {
                Token::Symbol(sym)
            }
        })
    }

    fn lex_ident_or_keyword(&mut self) -> Option<Token> {
        self.source_curr.chars().nth(0).and_then(|c|
            if !allowed_initially(c) {
                None
            } else {
                AfterCharIndices::new(self.source_curr).take_while(|&(_, v)| {
                    allowed(v)
                }).last().map(|(i, _)| {
                    let ident = &self.source_curr[..i];
                    self.advance(i);
                    ident
                })
            }
        ).map(|i| {
            KeywordToken::from_str(i).map(|x| { Token::Keyword(x) }).unwrap_or(Token::Ident(i))
        })

    }

    fn lex_whitespace(&mut self) -> Option<Token> {
        let mut max = 0;
        for x in AfterCharIndices::new(self.source_curr) {
            match x.1 {
                '\u{9}' | '\u{A}' | '\u{B}' | '\u{C}' | '\u{D}' | '\u{20}' |
                '\u{85}' | '\u{A0}' | '\u{1680}' | '\u{2000}' | '\u{2001}' | '\u{2002}' |
                '\u{2003}' | '\u{2004}' | '\u{2005}' | '\u{2006}' | '\u{2007}' | '\u{2008}' |
                '\u{2009}' | '\u{200A}' | '\u{200B}' | '\u{200C}' | '\u{200D}' | '\u{2028}' |
                '\u{2029}' | '\u{202F}' | '\u{205F}' | '\u{2060}' | '\u{3000}' => max = x.0,
                _ => break
            }
        }
        if max == 1 && self.source_curr.chars().nth(0) == Some(' ') {
            self.advance(max);
            Some(Token::Whitespace(SpaceKind::Space))
        } else if max != 0 {
            self.advance(max);
            Some(Token::Whitespace(SpaceKind::Other))
        } else {
            None
        }
    }

    fn generate_span(&mut self, ctx: &mut context::Context, from: &str) -> context::SpanId {
        let olp = self.source_start.as_ptr() as usize;
        let ohp = olp + self.source_start.len();
        let lip = from.as_ptr() as usize;
        let hip = self.source_curr.as_ptr() as usize;
        if olp <= lip && hip <= ohp {
            let lo = lip - olp;
            let hi = hip - olp;
            ctx.new_span(self.sid, lo, hi)
        } else {
            panic!("inner source locations are not inside the original source!");
        }
    }
}

pub trait LexerIterator {
    fn next(&mut self, ctx: &mut context::Context) -> Result<context::Spanned<Token>>;
    fn filter<F>(self, f: F) -> LexerFilter<Self, F>
    where F: Fn(&context::Spanned<Token>) -> bool, Self: Sized {
        LexerFilter {
            l: self,
            f: f
        }
    }
}

impl LexerIterator for Lexer {
    fn next(&mut self, ctx: &mut context::Context) -> Result<context::Spanned<Token>> {
        let token_lo = self.source_curr;
        self.try(Lexer::lex_documentation).or_else(|_|
            self.try(Lexer::lex_comment)
        ).or_else(|_|
            self.try(Lexer::lex_attribute)
        ).or_else(|_|
            self.try(Lexer::lex_symbol)
        ).or_else(|_|
            self.try(Lexer::lex_literal)
        ).or_else(|_|
            self.try(Lexer::lex_ident_or_keyword)
        ).or_else(|_|
            self.try(Lexer::lex_whitespace)
        ).or_else(|_| {
            if self.source_curr != "" {
                Err(Error::LexerError(LexerErrorKind::UnknownPrefix(
                    self.source_curr.chars().take(3).collect()
                )))
            } else {
                Ok(Token::EOF)
            }
        }).map(|t| {
            context::Spanned::new(t, self.generate_span(ctx, token_lo))
        })
    }
}

pub struct LexerFilter<L: LexerIterator, F: Fn(&context::Spanned<Token>) -> bool> {
    l: L,
    f: F
}

impl<I: LexerIterator, F: Fn(&context::Spanned<Token>) -> bool> LexerIterator for LexerFilter<I, F> {
    fn next(&mut self, ctx: &mut context::Context) -> Result<context::Spanned<Token>> {
        let n = try!(self.l.next(ctx));
        if (self.f)(&n) {
            Ok(n)
        } else {
            self.next(ctx)
        }
    }
}

// ---

struct AfterCharIndices<'a> {
    i: str::CharIndices<'a>
}

impl<'a> AfterCharIndices<'a> {
    fn new(i: &'a str) -> AfterCharIndices<'a> {
        AfterCharIndices {
            i: i.char_indices()
        }
    }
}

impl<'a> Iterator for AfterCharIndices<'a> {
    type Item = <str::CharIndices<'a> as Iterator>::Item;
    fn next(&mut self) -> Option<Self::Item> {
        self.i.next().map(|x| (x.0 + (x.1).len_utf8(), x.1))
    }
}

impl<'a> LexerIterator for ::std::slice::Iter<'a, context::Spanned<Token>> {
    fn next(&mut self, ctx: &mut context::Context) -> Result<context::Spanned<Token>> {
        Iterator::next(self).cloned().map(Ok).unwrap_or(Ok(context::Spanned {
            t: Token::EOE,
            span: ctx.dummy_span()
        }))
    }
}
